PRAGMA foreign_keys = OFF;

BEGIN TRANSACTION;

-- Table: SpellEffects
CREATE TABLE SpellEffects (
	Id        INTEGER NOT NULL
					  PRIMARY KEY AUTOINCREMENT,
	Spell     TEXT    NOT NULL,
	Target    INTEGER NOT NULL
					  REFERENCES BattleGladiators (Gladiator) ON UPDATE CASCADE
															  ON DELETE CASCADE,
	StartTurn INTEGER NOT NULL
);


-- Table: Battles
CREATE TABLE Battles (
	Id        INTEGER NOT NULL
					  PRIMARY KEY AUTOINCREMENT,
	Team1     INTEGER NOT NULL
					  REFERENCES Teams (Id) ON UPDATE CASCADE
											ON DELETE CASCADE,
	Team2     INTEGER NOT NULL
					  REFERENCES Teams (Id) ON UPDATE CASCADE
											ON DELETE CASCADE,
	Turn      INTEGER NOT NULL
	                  DEFAULT 1,
	Started   DATE    NOT NULL,
	Map       TEXT    NOT NULL,
	CombatLog TEXT    NOT NULL
	                  DEFAULT '',
	Chat      TEXT    NOT NULL
	                  DEFAULT ''
);


-- Table: Teams
CREATE TABLE Teams (
	Id      INTEGER NOT NULL
					PRIMARY KEY AUTOINCREMENT,
	User    INTEGER NOT NULL
					REFERENCES Accounts (Id) ON UPDATE CASCADE
											 ON DELETE CASCADE,
	Money   INTEGER
			DEFAULT 2000,
	Name    TEXT    NOT NULL
					UNIQUE,
	Battles INTEGER NOT NULL
					DEFAULT 0,
	Wins    INTEGER NOT NULL
					DEFAULT 0
);


-- Table: Gladiators
CREATE TABLE Gladiators (
	Id               INTEGER NOT NULL
							 PRIMARY KEY AUTOINCREMENT,
	Team             INTEGER NOT NULL
							 REFERENCES Teams (Id) ON UPDATE CASCADE
												   ON DELETE CASCADE,
	InStore          BOOLEAN NOT NULL,
	Name             TEXT    NOT NULL,
	Race             TEXT    NOT NULL,
	Salary           INTEGER NOT NULL,
	Age              INTEGER NOT NULL,
	Constitution     INTEGER NOT NULL,
	Strength         INTEGER NOT NULL,
	Agility          INTEGER NOT NULL,
	Wisdom           INTEGER NOT NULL,
	Dodge            INTEGER NOT NULL,
	MagicResistance  INTEGER NOT NULL,
	FistSkill	     INTEGER NOT NULL,
	SwordSkill       INTEGER NOT NULL,
	MaceSkill        INTEGER NOT NULL,
	SpearSkill       INTEGER NOT NULL,
	BowSkill         INTEGER NOT NULL,
	DestructionSkill INTEGER NOT NULL,
	RestorationSkill INTEGER NOT NULL,
	AlterationSkill  INTEGER NOT NULL,
	IllusionSkill    INTEGER NOT NULL,
	MeleeWeapon		 TEXT,
	RangedWeapon     TEXT,
	Armor            TEXT,
	SpellOne         TEXT,
	SpellTwo         TEXT,
	SpellThree       TEXT,
	Coma             INTEGER NOT NULL
);


-- Table: BattleGladiators
CREATE TABLE BattleGladiators (
	Battle    INTEGER NOT NULL
					  REFERENCES Battles (Id) ON UPDATE CASCADE
											  ON DELETE CASCADE,
	Gladiator INTEGER NOT NULL
					  UNIQUE
					  REFERENCES Gladiators (Id) ON UPDATE CASCADE
												 ON DELETE CASCADE,
	Turn      INTEGER NOT NULL,
	HP        INTEGER NOT NULL,
	Mana      INTEGER NOT NULL,
	PosX      INTEGER NOT NULL,
	PosY      INTEGER NOT NULL
);


-- Table: Accounts
CREATE TABLE Accounts (
	Id       INTEGER NOT NULL
					 PRIMARY KEY AUTOINCREMENT,
	Username TEXT    NOT NULL,
	Password TEXT    NOT NULL,
	Email    TEXT    NOT NULL,
	Level    INTEGER NOT NULL,
	BanTime  INTEGER NOT NULL
					 DEFAULT -1
);

-- Data

INSERT INTO Accounts (Id, Username, Password, Email, Level, BanTime)
VALUES (1, 'aleksi', '$2y$10$rv7lCkGo2lqvh8.ECdFINeIyeQX.lT6PDB1afBwtYJuS6dB29Uwv2', 'not@real.com', 2, -1);

INSERT INTO Accounts (Id, Username, Password, Email, Level, BanTime)
VALUES (2, 'Topium', '$2y$10$rmW6fhJuAfNPA7lnhF9WTeX3/QUvsolP6O6JPyVgu.3pX3JO.CUdO', 'fake@fake', 2, -1);

INSERT INTO Accounts (Id, Username, Password, Email, Level, BanTime)
VALUES (3, 'emuski95', '$2y$10$fYBuheJ0pHHMJFr9HSbl7OFeSv5ayWcjCx26Ct1BUzS88vLbGQ9sS', 'fake@fake', 2, -1);

-- Teams
INSERT INTO Teams (Id, User, Money, Name, Battles, Wins)
VALUES (1, 1, 2000, 'aleksin', 0, 0);

INSERT INTO Teams (Id, User, Money, Name, Battles, Wins)
VALUES (2, 2, 2000, 'topin', 0, 0);

-- Gladiators
INSERT INTO Gladiators (Id, Team, InStore, Name, Race, Age, Salary, Constitution, Strength, Agility,
                        Wisdom, Dodge, MagicResistance, FistSkill, SwordSkill, MaceSkill, SpearSkill,
                        BowSkill, DestructionSkill, RestorationSkill, AlterationSkill, IllusionSkill,
                        MeleeWeapon, RangedWeapon, Armor, SpellOne, SpellTwo, SpellThree, Coma)
VALUES (1, 1, 0, 'Crash', 'tite', 30, 50, 22, 22, 22,
        22, 22, 22, 22, 22, 22, 22,
        22, 22, 22, 22, 22,
        'teravaKeppi', 'lyhytjousi', 'nahkanuttu', 'haava', 'auttavaKasi', 'lentavaSika', 0);

INSERT INTO Gladiators (Id, Team, InStore, Name, Race, Age, Salary, Constitution, Strength, Agility,
                        Wisdom, Dodge, MagicResistance, FistSkill, SwordSkill, MaceSkill, SpearSkill,
                        BowSkill, DestructionSkill, RestorationSkill, AlterationSkill, IllusionSkill,
                        MeleeWeapon, RangedWeapon, Armor, SpellOne, SpellTwo, SpellThree, Coma)
VALUES (3, 1, 0, 'Test', 'tite', 30, 50, 22, 22, 22,
        22, 22, 22, 22, 22, 22, 22,
        22, 22, 22, 22, 22,
        'teravaKeppi', 'lyhytjousi', 'nahkanuttu', 'mielenrauha', 'nikotuskohtaus', 'nikotuskohtaus', 0);

INSERT INTO Gladiators (Id, Team, InStore, Name, Race, Age, Salary, Constitution, Strength, Agility,
                        Wisdom, Dodge, MagicResistance, FistSkill, SwordSkill, MaceSkill, SpearSkill,
                        BowSkill, DestructionSkill, RestorationSkill, AlterationSkill, IllusionSkill,
                        MeleeWeapon, RangedWeapon, Armor, SpellOne, SpellTwo, SpellThree, Coma)
VALUES (2, 2, 0, 'Dummy', 'tite', 30, 50, 22, 22, 21,
        22, 22, 22, 22, 22, 22, 22,
        22, 22, 22, 22, 22,
        'mahtimoukari', 'lyhytjousi', 'mystinenHaarniska', 'mielenrauha', 'neliapila', 'nikotuskohtaus', 0);

COMMIT TRANSACTION;