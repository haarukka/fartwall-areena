<?php
/**
 * Created by IntelliJ IDEA.
 * User: topi
 * Date: 6.12.2014
 * Time: 11:05
 */

require (APP . 'game/statgenerator.php');


class Gladiators
{
	public $inStore = true;
	public $name;
	public $race;
	public $salary;
	public $age;
	public $constitution;
	public $strength;
	public $agility;
	public $wisdom;
	public $dodge;
	public $magicRes;
	public $fist;
	public $sword;
	public $mace;
	public $spear;
	public $bow;
	public $destruction;
	public $restoration;
	public $alteration;
	public $illusion;
	public $meleeweapon;
	public $rangedweapon;
	public $armor;
	public $spellone;
	public $spelltwo;
	public $spellthree;
	public $coma = 0;

	public function __construct()
	{
		$stats = new StatGenerator();
		$this->name = $stats->getName();
		$this->race = $stats->getRace();
		$this->salary = $stats->getSalary();
		$this->age = $stats->getAge();
		$this->constitution = $stats->getConstitution($this->race);
		$this->strength = $stats->getStrength($this->race);
		$this->agility = $stats->getAgility($this->race);
		$this->wisdom = $stats->getWisdom($this->race);
		$this->dodge = $stats->getAgility($this->race);
		$this->magicRes = $stats->getWisdom($this->race) + 15;
		$this->fist = $stats->getFistSkill($this->race) + $this->agility;
		$this->sword = $stats->getSwordSkill($this->race) + $this->agility;
		$this->mace = $stats->getMaceSkill($this->race) + $this->agility;
		$this->spear = $stats->getSpearSkill($this->race) + $this->agility;
		$this->bow = $stats->getBowSkill($this->race) + $this->agility;
		$this->destruction = $stats->getSpellSkill($this->race) + $this->wisdom;
		$this->restoration = $stats->getSpellSkill($this->race) + $this->wisdom;
		$this->alteration = $stats->getSpellSkill($this->race) + $this->wisdom;
		$this->illusion = $stats->getSpellSkill($this->race) + $this->wisdom;
	}
}