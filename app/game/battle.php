<?php

require_once(APP . 'models/team.php');
require_once(APP . 'models/battle.php');
require_once(APP . 'lib/newgladiator.php');

/**
 * @var $weapons Weapon[]
 */
$weapons = require(APP . 'game/weapons.php');

/**
 * @var $armors Armor[]
 */
$armors = require(APP . 'game/armors.php');

function sortByAgility($a, $b) {
	return $b[1] - $a[1];
}

class BattleLogic
{
	/**
	 * @var BattleModel
	 */
	private $model;
	private $teamModel;

	/**
	 * @var Battle
	 */
	public $battle;

	public $gladiators;

	/**
	 * @param $battleMdl BattleModel
	 * @param $teamMdl TeamModel
	 * @param $battle Battle
	 */
	public function __construct($battleMdl, $teamMdl, $battle) {
		$this->model = $battleMdl;
		$this->teamModel = $teamMdl;
		$this->battle = $battle;

		$this->gladiators = $battleMdl->getGladiators($battle->Id);
	}

	/**
	 * @param $battleMdl BattleModel
	 * @param $teamMdl TeamModel
	 * @param $team1 Team
	 * @param $team2 Team
	 * @return BattleLogic
	 */
	public static function createBattle($battleMdl, $teamMdl, $team1, $team2) {
		$id = $battleMdl->addBattle($team1->Id, $team2->Id);

		$bl = new BattleLogic($battleMdl, $teamMdl, $battleMdl->getBattle($id));
		$bl->calculateTurnOrder();

		return $bl;
	}

	private function calculateTurnOrder() {
		$turns = array();

		foreach($this->gladiators as $glad) {
			$turns[] = array($glad->Gladiator->Id, $glad->Gladiator->Agility);
		}

		usort($turns, 'sortByAgility');

		foreach($turns as $turn => $glad) {
			$this->model->setGladiatorTurn($glad[0], $turn + 1);
		}
	}

	private function addWinLoss($winner, $loser) {
		$winner = $this->teamModel->getTeamData($winner);
		$loser = $this->teamModel->getTeamData($loser);

		$this->teamModel->updateTeam($loser->Id,
			$loser->Money + mt_rand(50, 100),
			$loser->Battles + 1,
			$loser->Wins);
		$this->teamModel->updateTeam($winner->Id,
			$winner->Money + mt_rand(100, 300),
			$winner->Battles + 1,
			$winner->Wins + 1);

		regenerateGladiators($this->model->getDb(), $winner->Id);
		regenerateGladiators($this->model->getDb(), $loser->Id);
	}

	/**
	 * @param $x
	 * @param $y
	 * @return BattleGladiator
	 */
	public function checkPosition($x, $y) {
		return $this->model->checkPosition($this->battle->Id, $x, $y);
	}

	/**
	 * @param $id
	 * @param $teamMdl TeamModel
	 * @return BattleGladiator
	 */
	private function checkActive($id, $teamMdl) {
		$active = $this->model->getActiveGladiator($this->battle->Id);

		if (!$active) return false;

		$team = $teamMdl->getTeamData($active->Gladiator->Team);

		if ($team && $team->User === $id) {
			return $active;
		}
		return false;
	}

	private function endTurn() {
		$active = false;

		$team1Alive = $this->model->getAliveAmount(
			$this->battle->Id,
			$this->battle->Team1);
		$team2Alive = $this->model->getAliveAmount(
			$this->battle->Id,
			$this->battle->Team2);

		if ($team1Alive === 0 || $team2Alive === 0) {
			if ($team1Alive === 0) {
				$winner = $this->battle->Team2;
				$loser = $this->battle->Team1;
			} else {
				$winner = $this->battle->Team1;
				$loser = $this->battle->Team2;
			}

			$this->addWinLoss($winner, $loser);

			$this->model->deleteBattle($this->battle->Id);
			return;
		}

		while (!$active) {
			$this->model->incrementTurn($this->battle->Id);
			$active = $this->model->getActiveGladiator($this->battle->Id);
		}
	}

	/**
	 * @param $active BattleGladiator
	 * @param $x
	 * @param $y
	 * @param $range
	 * @return bool
	 */
	private function isInRange($active, $x, $y, $range) {
		return (abs($x - $active->PosX) <= $range
			 && abs($y - $active->PosY) <= $range);
	}

	/**
	 * @param $id
	 * @param $teamMdl TeamModel
	 */
	public function forfeit($id, $teamMdl) {
		$team1 = $teamMdl->getTeamData($this->battle->Team1);
		$team2 = $teamMdl->getTeamData($this->battle->Team2);

		if ($id !== $team1->User && $id !== $team2->User) die();

		$this->model->deleteBattle($this->battle->Id);

		if ($id === $team1->User) {
			$this->addWinLoss($team2->Id, $team1->Id);
		} else {
			$this->addWinLoss($team1->Id, $team2->Id);
		}
	}

	/**
	 * @param $x
	 * @param $y
	 * @param $id
	 * @param $teamMdl TeamModel
	 */
	public function move($x, $y, $id, $teamMdl) {
		$active = $this->checkActive($id, $teamMdl);

		if (!$active) return;

		if ($this->isInRange($active, $x, $y, 1)) {
			if (!$this->checkPosition($x, $y)) {
				$this->model->setGladiatorPos($active->Gladiator->Id, $x, $y);
				$this->endTurn();
			}
		}
	}

	/**
	 * @param $x
	 * @param $y
	 * @param $id
	 * @param $teamMdl TeamModel
	 */
	public function melee($x, $y, $id, $teamMdl) {
		global $weapons, $armors;

		$active = $this->checkActive($id, $teamMdl);

		if (!$active) return;

		$enemy = $this->checkPosition($x, $y);
		if (!$enemy
			|| $teamMdl->getTeamData($enemy->Gladiator->Team)->User === $id) {
			return;
		}

		if (!$this->isInRange($active, $x, $y, 1)) return;

		$ownWep = $active->Gladiator->MeleeWeapon;
		$ownWep = getWeaponByName($weapons, $ownWep);

		$enemyWep = $enemy->Gladiator->MeleeWeapon;
		$enemyWep = getWeaponByName($weapons, $enemyWep);

		$dmg = $ownWep->rollDamage();
		$block = $enemyWep->checkBlock();

		if ($block) {
			$dmg = 0;
		}

		$enemyArmor = $enemy->Gladiator->Armor;
		if (isset($armors[$enemyArmor])) {
			$dmg = $armors[$enemyArmor]->reduceDmg($dmg);
		}

		$this->model->hurtGladiator($enemy->Gladiator->Id, $dmg);

		$this->endTurn();
	}
}