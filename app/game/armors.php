<?php

class Armor {
	private $name;
	private $ac;
	private $price;

	public function __construct($name, $ac, $price) {
		$this->name = $name;
		$this->ac = $ac;
		$this->price = $price;
	}

	public function getName() {
		return $this->name;
	}

	public function getAc() {
		return $this->ac;
	}

	public function getPrice() {
		return $this->price;
	}

	public function reduceDmg($dmg) {
		return max(0, $dmg - $this->ac);
	}
}

return array(
	// Armors
	'nahkanuttu' => new Armor('Nahkanuttu',1 ,32),
	'besaittuhaarniska' => new Armor('Besaittuhaarniska', 2, 128),
	'silmukkahaarniska' => new Armor('Silmukkahaarniska', 3, 288),
	'rengashaarniska' => new Armor('Rengashaarniska', 4, 512),
	'suomuhaarniska' => new Armor('Suomuhaarniska', 5, 800),
	'levyhaarniska' => new Armor('Levyhaarniska',6 , 1152),
	'mystinenHaarniska' => new Armor('Mystinen haarniska',7 , 1568),
	'mithrilhaarniska' => new Armor('Mithrilhaarniska', 8, 2048),
	'siunattuHaarniska' => new Armor('Siunattu haarniska', 10, 3200),
	'mahtihaarniska' => new Armor('Mahtihaarniska', 12, 4608)
);