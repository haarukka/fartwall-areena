<?php

abstract class Spell {
	private $name;
	private $price;

	public function __construct($name, $price) {
		$this->name = $name;
		$this->price = $price;
	}

	public function getName() {
		return $this->name;
	}

	public function getPrice() {
		return $this->price;
	}

	abstract public function applyEffect($target);
}

class DamageSpell extends Spell {
	private $minDmg, $maxDmg;

	public function __construct($name, $minDmg, $maxDmg, $price) {
		parent::__construct($name, $price);

		$this->minDmg = $minDmg;
		$this->maxDmg = $maxDmg;
	}

	public function getDamage() {
		return array($this->minDmg, $this->maxDmg);
	}

	public function applyEffect($target) {
		// deal damage
	}
}

class HealSpell extends Spell {
	private $healAmount;

	public function __construct($name, $heal, $price) {
		parent::__construct($name, $price);

		$this->healAmount = $heal;
	}

	public function getHealAmount() {
		return $this->healAmount;
	}

	public function applyEffect($target) {
		// heal target
	}
}

class BuffSpell extends Spell {
	private $buffAmount;
	
	public function __construct($name, $buff, $price) {
		parent::__construct($name, $price);
		
		$this->buffAmount = $buff;
	}
	
	public function getBuffAmount() {
		return $this->buffAmount;
	}
	
	
	public function applyEffect($target) {
		// apply buffs
	}
}

class IllusionSpell extends Spell {
	private $stunTime;
	
	public function __construct($name, $time, $price) {
		parent::__construct($name, $price);
		
		$this->stunTime = $time;
	}
	
	public function getStunTime() {
		return $this->stunTime;
	}
	
	public function applyEffect($target) {
		// Apply effect
	}
}


return array(

'damageSpells' => array(
// Damagespells
'haava' => new DamageSpell('Haava', 1, 3, 100),
'vesivasama' => new DamageSpell('Vesivasama', 2, 6, 200),
'valovasama' => new DamageSpell('Valovasama', 3, 9, 300),
'jaatavaPolte' => new DamageSpell('Jäätävä polte', 4, 12, 400),
'turjake' => new DamageSpell('Turjake', 5, 15, 500),
'synkkaTuomio' => new DamageSpell('Synkkä tuomio', 6, 18, 600)

),
'healSpells' => array (
// HealSpells
'auttavaKasi' => new HealSpell('Auttava käsi', 5, 100),
'henkinenParannus' => new HealSpell('Henkinen parannus', 10, 200),
'mielenrauha' => new HealSpell('Mielenrauha', 15, 300),
'parannusvakuutus' => new HealSpell('Parannusvakuutus', 20, 400),
'tosiTerveys' => new HealSpell('Tosi terveys', 25, 500),
'pappisparannus' => new HealSpell('Pappisparannus', 30, 600)

),
 'buffSpells' => array (
// Buffspells
'onnentoivotus' => new BuffSpell('Onnentoivotus', 5, 100),
'janiksenkapala' => new BuffSpell('Jäniksenkäpälä', 10, 200),
'neliapila' => new BuffSpell('Neliapila', 15, 300),
'tosiSiunaus' => new BuffSpell('Tosi siunaus', 20, 400)

 ),
 'illusionSpells' => array (
// Illusionspells
'lentavaSika' => new IllusionSpell('Lentävä sika', 1, 200),
'nikotuskohtaus' => new IllusionSpell('Nikotuskohtaus', 2, 400)

 ));