<?php

class StatGenerator
{
	public function getName()
	{
		$names = array("Alexius", "Anselmo", "Asmund", "Booris", "Dublin", "Enska", "Feelix", "Fredi", "Hannibal",
			"Heikkijaakko", "Vilho-Oliver", "Iippo", "Jaska", "Pellervo", "Kustaa", "Jeppe", "Jurkka", "Kalervo", "Kössi",
			"Leonartti", "Mortti", "Masa", "Nakke", "Nipa", "Obi", "Pate", "Pellonpekko", "Perttyli", "Quinn",
			"Ruurikki", "Ransu", "Reinari", "Simojussi", "Sepeteus", "Timotei", "Tapsa", "Villehart",
			"Vilppo", "Xan", "Yrmy", "Zen");
		return $names[mt_rand(1, count($names))];
	}

	public function getRace()
	{
		$rand = mt_rand(1, 5);
		switch ($rand) {
			case 1:
				return "Tite";
			case 2:
				return "Kete";
			case 3:
				return "Tuta";
			case 4:
				return "Kote";
			case 5:
				return "Super amis";
			default:
				return 0;
		}
	}


	public function getAge()
	{
		return mt_rand(20, 35);
	}

	public function getConstitution($race)
	{
		switch ($race) {
			case "Tite":
				return mt_rand(19, 25);
			case "Kete":
				return mt_rand(12, 17);
			case "Tuta":
				return mt_rand(10, 15);
			case "Kote":
				return mt_rand(14, 19);
			case "Super amis":
				return mt_rand(22, 28);
			default:
				return 0;
		}
	}

	public function getStrength($race)
	{
		switch ($race) {
			case "Tite":
				return mt_rand(10, 20);
			case "Kete":
				return mt_rand(5, 15);
			case "Tuta":
				return mt_rand(1, 15);
			case "Kote":
				return mt_rand(5, 20);
			case "Super amis":
				return mt_rand(10, 25);
			default:
				return 0;
		}
	}

	public function getAgility($race)
	{
		switch ($race) {
			case "Tite":
				return mt_rand(12, 17);
			case "Kete":
				return mt_rand(12, 22);
			case "Tuta":
				return mt_rand(12, 22);
			case "Kote":
				return mt_rand(8, 21);
			case "Super amis":
				return mt_rand(5, 12);
			default:
				return 0;
		}
	}

	public function getWisdom($race)
	{
		switch ($race) {
			case "Tite":
				return mt_rand(9, 14);
			case "Kete":
				return mt_rand(18, 24);
			case "Tuta":
				return mt_rand(22, 26);
			case "Kote":
				return mt_rand(12, 18);
			case "Super amis":
				return mt_rand(6, 9);
			default:
				return 0;
		}
	}

	public function getBowSkill($race)
	{
		switch ($race) {
			case "Tite":
				return 1;
			case "Kete":
				return mt_rand(15, 25);
			case "Tuta":
				return mt_rand(1, 5);
			case "Kote":
				return mt_rand(2, 10);
			case "Super amis":
				return 1;
			default:
				return 0;
		}
	}

	public function getFistSkill($race)
	{
		switch ($race) {
			case "Tite":
				return mt_rand(20, 25);
			case "Kete":
				return mt_rand(20, 25);
			case "Tuta":
				return mt_rand(15, 20);
			case "Kote":
				return mt_rand(15, 20);
			case "Super amis":
				return mt_rand(25, 30);
			default:
				return 0;
		}
	}

	public function getSwordSkill($race)
	{
		switch ($race) {
			case "Tite":
				return mt_rand(10, 15);
			case "Kete":
				return mt_rand(5, 8);
			case "Tuta":
				return mt_rand(1, 5);
			case "Kote":
				return mt_rand(5, 8);
			case "Super amis":
				return mt_rand(5, 10);
			default:
				return 0;
		}
	}

	public function getSpearSkill($race)
	{
		switch ($race) {
			case "Tite":
				return mt_rand(7, 12);
			case "Kete":
				return mt_rand(2, 5);
			case "Tuta":
				return mt_rand(1, 5);
			case "Kote":
				return 1;
			case "Super amis":
				return mt_rand(10, 20);
			default:
				return 0;
		}
	}

	public function getMaceSkill($race)
	{
		switch ($race) {
			case "Tite":
				return mt_rand(4, 10);
			case "Kete":
				return 1;
			case "Tuta":
				return 1;
			case "Kote":
				return mt_rand(2, 10);
			case "Super amis":
				return mt_rand(15, 25);
			default:
				return 0;
		}
	}

	public function getSpellSkill($race)
	{
		switch ($race) {
			case "Tite":
				return mt_rand(10, 20);
			case "Kete":
				return mt_rand(10, 15);
			case "Tuta":
				return mt_rand(10, 20);
			case "Kote":
				return mt_rand(10, 15);
			case "Super amis":
				return mt_rand(1, 10);
			default:
				return 0;
		}
	}

	public function getSalary()
	{
		return 50;
	}
}