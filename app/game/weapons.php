<?php


abstract class Weapon {
	private $name;
	private $minDmg, $maxDmg;
	private $block;
	private $price;

	public function __construct($name, $minDmg, $maxDmg, $block, $price) {
		$this->name = $name;
		$this->minDmg = $minDmg;
		$this->maxDmg = $maxDmg;
		$this->block = $block;
		$this->price = $price;
	}

	public function getName() {
		return $this->name;
	}

	public function getDamage() {
		return array($this->minDmg, $this->maxDmg);
	}

	public function getBlock() {
		return $this->block;
	}

	public function getPrice() {
		return $this->price;
	}

	public function rollDamage() {
		$dmg = mt_rand($this->minDmg, $this->maxDmg);
		if (mt_rand(0, 100) <= 5) {
			$dmg = floor($dmg * 1.5);
		}
		return $dmg;
	}

	public function checkBlock() {
		// return true to block
		return mt_rand(0, 100) < ($this->block * 100);
	}
}

class Fist extends Weapon {
}

class Spear extends Weapon {
}

class Sword extends Weapon {
}

class Mace extends Weapon {
}

class Bow extends Weapon {
}

/**
 * @param $weps Weapon[]
 * @param $name
 * @return Weapon
 */
function getWeaponByName($weps, $name) {
	foreach($weps as $cat) {
		foreach($cat as $k => $w) {
			if ($k === $name) {
				return $w;
			}
		}
	}

	return $weps['fists']['fist'];
}

return array(
	'fists' => array(
		// Fist
		'fist' => new Fist('Nyrkki', 1, 2, 0, 0)),
	'spears' => array(
		// Spears
		'teravaKeppi' => new Spear('Terävä keppi', 2, 5, 0.05, 40),
		'kivikeihas' => new Spear('Kivikeihäs', 2, 7, 0.075, 105),
		'lyhytKeihas' => new Spear('Lyhyt keihäs', 2, 9, 0.1, 205),
		'pitkaKeihas' => new Spear('Pitkä keihäs', 2, 11, 0.125, 328),
		'suurkeihas' => new Spear('Suurkeihäs', 4, 14, 0.15, 683),
		'jalkavaenPeitsi' => new Spear('Jalkaväen peitsi', 3, 18, 0.175, 822),
		'mystinenKeihas' => new Spear('Mystinen keihäs', 6, 18, 0.2, 1195),
		'tulikeihas' => new Spear('Tulikeihäs', 7, 22, 0.225, 1504),
		'siunattuKeihas' => new Spear('Siunattu keihäs', 8, 29, 0.25, 2433),
		'mahtikeihas' => new Spear('Mahtikeihäs', 12, 30, 0.275, 3535)),
	'swords' => array (
		// Swords
		'tikari' => new Sword('Tikari', 2, 5, 0.1, 71),
		'lyhytMiekka' => new Sword('Lyhyt miekka', 2, 7, 0.13, 160),
		'sapeli' => new Sword('Sapeli', 3, 8, 0.16, 230),
		'leveaMiekka' => new Sword('Leveä miekka', 3, 10, 0.19, 360),
		'bastardiMiekka' => new Sword('Bastardi miekka', 3, 12, 0.22, 480),
		'suurmiekka' => new Sword('Suurmiekka', 4, 14, 0.25, 683),
		'mustaMiekka' => new Sword('Musta miekka', 9, 16, 0.28, 1195),
		'tulimiekka' => new Sword('Tulimiekka', 6, 20, 0.31, 1672),
		'siunattuMiekka' => new Sword('Siunattu miekka', 6, 27, 0.34, 2252),
		'mahtimiekka' => new Sword('Mahtimiekka', 12, 30, 0.37, 3946)),
	'maces' => array (
		// Maces
		'puunuija' => new Mace('Puunuija', 1, 4, 0.1, 25),
		'pamppu' => new Mace('Pamppu', 1, 6, 0.125, 102),
		'nuija' => new Mace('Nuija', 1, 8, 0.15, 205),
		'raskasNuija' => new Mace('Raskas nuija', 1, 10, 0.175, 360),
		'moukari' => new Mace('Moukari', 2, 12, 0.2, 577),
		'sotamoukari' => new Mace('Sotamoukari', 3, 18, 0.225, 1137),
		'lumottuNuija' => new Mace('Lumottu nuija', 2, 16, 0.25, 1849),
		'muinainenNuija' => new Mace('Muinainen nuija', 9, 19, 0.275, 1886),
		'siunattuNuija' => new Mace('Siunattu nuija', 11, 26, 0.3, 3635),
		'mahtimoukari' => new Mace('Mahtimoukari', 12, 30, 0.325, 4898)),
	'bows' => array (
		// Bows
		'lyhytjousi' => new Bow('Lyhytjousi', 2, 5, 0, 172),
		'orkkijousi' => new Bow('Örkkijousi', 2, 7, 0, 306),
		'pitkajousi' => new Bow('Pitkäjousi', 2, 9, 0, 478),
		'sarvijousi' => new Bow('Sarvijousi', 2, 11, 0, 688),
		'haltiajousi' => new Bow('Haltiajousi', 2, 13, 0, 937),
		'mestarijousi' => new Bow('Mestarijousi', 4, 14, 0, 1509),
		'lumottuJousi' => new Bow('Lumottu jousi', 6, 15, 0, 1913),
		'pyhaHaltiajousi' => new Bow('Pyhä haltiajousi', 6, 21, 0, 2755),
		'siunattuJousi' => new Bow('Siunattu jousi', 10, 21, 0, 3233),
		'mahtijousi' => new Bow('Mahtijousi', 9, 25, 0, 4898))
);