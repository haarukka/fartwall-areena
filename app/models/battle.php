<?php

require_once(APP . 'lib/model.php');
require(APP . 'models/gladiators.php');

class Battle
{
	public $Id;
	public $Team1;
	public $Team2;
	public $Turn;
	public $Started;
	public $Map;
	public $CombatLog;
	public $Chat;

	public function __construct()
	{
		$this->Id = intval($this->Id);
		$this->Team1 = intval($this->Team1);
		$this->Team2 = intval($this->Team2);
		$this->Turn = intval($this->Turn);
		$this->Started = strtotime($this->Started);
	}
}

class BattleGladiator
{
	public $Gladiator;
	public $Battle;
	public $Turn;
	public $HP;
	public $Mana;
	public $PosX;
	public $PosY;

	public function __construct($db) {
		$mdl = new GladiatorModel($db);

		$this->Gladiator = $mdl->getGladiator(intval($this->Gladiator));
		$this->Battle = intval($this->Battle);
		$this->Turn = intval($this->Turn);
		$this->HP = intval($this->HP);
		$this->Mana = intval($this->Mana);
		$this->PosX = intval($this->PosX);
		$this->PosY = intval($this->PosY);
	}
}

class BattleModel extends Model
{
	public function addBattle($team1, $team2) {
		$stmt = $this->db->prepare("
		INSERT INTO Battles (Team1, Team2, Started, Map)
		VALUES (:team1, :team2, datetime('now'), 'fartwall')");
		$stmt->bindParam(':team1', $team1);
		$stmt->bindParam(':team2', $team2);
		$stmt->execute();

		$id = $this->db->lastInsertId();

		$this->activateGladiators($id, $team1, $team2);

		return $id;
	}

	public function deleteBattle($id) {
		$stmt = $this->db->prepare("
			DELETE FROM Battles
			WHERE Id = :id");
		$stmt->bindParam(':id', $id);
		$stmt->execute();
	}

	public function incrementTurn($id) {
		$stmt = $this->db->prepare("
			UPDATE Battles
			SET Turn = Turn + 1
			WHERE Id = :id");
		$stmt->bindParam(':id', $id);
		$stmt->execute();
	}

	private function activateGladiators($battle, $team1, $team2) {
		$stmt = $this->db->prepare('
			INSERT INTO BattleGladiators (Battle, Gladiator, Turn, HP, Mana, PosX, PosY)
			SELECT :battle, G.Id, 0, G.Constitution, G.Wisdom, 1, 1
			FROM Gladiators G
			WHERE NOT G.InStore AND (G.Team = :team1 OR G.Team = :team2)
		');
		$stmt->bindParam(':battle', $battle);
		$stmt->bindParam(':team1', $team1);
		$stmt->bindParam(':team2', $team2);
		$stmt->execute();

		$glads1 = $this->getTeamGladiators($battle, $team1);
		$glads2 = $this->getTeamGladiators($battle, $team2);

		$y = 1;
		foreach($glads1 as $g) {
			$this->setGladiatorPos($g->Gladiator->Id, 1, $y++);
		}

		$y = 1;
		foreach($glads2 as $g) {
			$this->setGladiatorPos($g->Gladiator->Id, 10, $y++);
		}
	}

	public function isUserInBattle($id) {
		$stmt = $this->db->prepare("
			SELECT EXISTS(
				SELECT 1
				FROM Battles
				LEFT JOIN Teams T
					ON T.Id = Battles.Team1
						OR T.Id = Battles.Team2
				WHERE
					T.User = :id
			)");
		$stmt->bindParam(':id', $id);
		$stmt->execute();

		$res = $stmt->fetch();
		return $res[0] === '1';
	}

	public function getAllBattles() {
		$stmt = $this->db->prepare('SELECT * FROM Battles');
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_CLASS, 'Battle');
	}

	public function getBattle($id) {
		$stmt = $this->db->prepare('SELECT * FROM Battles WHERE Id = :id');
		$stmt->bindParam(':id', $id);
		$stmt->execute();
		$stmt->setFetchMode(PDO::FETCH_CLASS, 'Battle');
		return $stmt->fetch();
	}

	public function getGladiators($battle) {
		$stmt = $this->db->prepare('SELECT * FROM BattleGladiators WHERE Battle = :battle');
		$stmt->bindParam(':battle', $battle);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_CLASS, 'BattleGladiator', array($this->db));
	}

	public function getTeamGladiators($battle, $team) {
		$stmt = $this->db->prepare('
			SELECT *
			FROM BattleGladiators BG
			INNER JOIN Gladiators G
			ON G.Id = BG.Gladiator
			WHERE BG.Battle = :battle
			      AND G.Team = :team');
		$stmt->bindParam(':battle', $battle);
		$stmt->bindParam(':team', $team);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_CLASS, 'BattleGladiator', array($this->db));
	}

	public function getActiveAmount($battle) {
		$stmt = $this->db->prepare('
			SELECT COUNT(*)
			FROM BattleGladiators
			WHERE Battle = :id');
		$stmt->bindParam(':id', $battle);
		$stmt->execute();
		return $stmt->fetch();
	}

	public function getAliveAmount($battle, $team) {
		$stmt = $this->db->prepare('
			SELECT COUNT(*)
			FROM BattleGladiators BG
			INNER JOIN Gladiators G
			ON G.Id = BG.Gladiator
			WHERE BG.Battle = :battle
			      AND G.Team = :team
			      AND BG.HP > 0');
		$stmt->bindParam(':battle', $battle);
		$stmt->bindParam(':team', $team);
		$stmt->execute();
		$ret = $stmt->fetch();
		return intval($ret[0]);
	}

	public function getActiveGladiator($battle) {
		$am = $this->getActiveAmount($battle);
		$am = $am[0];
		$stmt = $this->db->prepare('
			SELECT *
			FROM BattleGladiators
			WHERE HP > 0 AND Turn IN
			(
				SELECT ((Turn - 1) % :am) + 1
				FROM Battles
				WHERE Id = :id
			)');
		$stmt->bindParam(':id', $battle);
		$stmt->bindParam(':am', $am);
		$stmt->execute();
		$stmt->setFetchMode(PDO::FETCH_CLASS, 'BattleGladiator', array($this->db));
		return $stmt->fetch();
	}

	/**
	 * @param $id
	 * @param $x
	 * @param $y
	 * @return BattleGladiator
	 */
	public function checkPosition($id, $x, $y) {
		$stmt = $this->db->prepare("
			SELECT *
			FROM BattleGladiators
			WHERE Battle = :id AND PosX = :x AND PosY = :y");
		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':x', $x);
		$stmt->bindParam(':y', $y);
		$stmt->execute();

		$stmt->setFetchMode(PDO::FETCH_CLASS, 'BattleGladiator', array($this->db));
		return $stmt->fetch();
	}

	public function setGladiatorTurn($glad, $turn) {
		$stmt = $this->db->prepare('UPDATE BattleGladiators SET Turn = :turn WHERE Gladiator = :id');
		$stmt->bindParam(':id', $glad);
		$stmt->bindParam(':turn', $turn);
		$stmt->execute();
	}

	public function setGladiatorPos($glad, $x, $y) {
		$stmt = $this->db->prepare('UPDATE BattleGladiators
			SET PosX = :x,
			    PosY = :y
			WHERE Gladiator = :id');

		$stmt->bindParam(':id', $glad);
		$stmt->bindParam(':x', $x);
		$stmt->bindParam(':y', $y);
		$stmt->execute();
	}

	public function hurtGladiator($glad, $dmg) {
		$stmt = $this->db->prepare('UPDATE BattleGladiators
			SET HP = HP - :dmg
			WHERE Gladiator = :id');

		$stmt->bindParam(':id', $glad);
		$stmt->bindParam(':dmg', $dmg);
		$stmt->execute();
	}
}