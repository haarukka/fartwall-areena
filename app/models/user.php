<?php

require_once(APP . 'lib/model.php');

abstract class UserGroup {
	const Banned = 0;
	const Normal = 1;
	const Admin = 2;
}

class User
{
	public $Id;
	public $Username;
	public $Password;
	public $Email;
	public $Level;
	public $BanTime;

	public function __construct() {
		$this->Id = intval($this->Id);
		$this->Level = intval($this->Level);
		$this->BanTime = intval($this->BanTime);
	}
}

class UserModel extends Model {

	private function generateSalt($length) {
		// Generate a random binary string
		if (function_exists('mcrypt_create_iv')) {
			$bytes = mcrypt_create_iv($length, MCRYPT_DEV_URANDOM);
		} else {
			$bytes = openssl_random_pseudo_bytes($length);
		}

		// Limit the base64 result to 22 characters
		$saltBase64 = substr(base64_encode($bytes), 0, 22);
		// And replace invalid characters (plus isn't allowed in salts)
		return strtr($saltBase64, '+', '.');
	}

	private function hashPassword($password) {
		$salt = $this->generateSalt(22);

		// How many iterations crypt will do (2^cost)
		$cost = 10;

		// Build a Blowfish salt
		$bfSalt = sprintf('$2y$%d$%s$', $cost, $salt);

		return crypt($password, $bfSalt);
	}

	public function getAllUsers()
	{
		$stmt = $this->db->prepare('SELECT * FROM Accounts');
		$stmt->execute();
		$stmt->setFetchMode(PDO::FETCH_CLASS, 'User');

		return $stmt->fetchAll();
	}

	/**
	 * @param $id integer User's ID
	 * @return User
	 */
	public function getUser($id) {
		$stmt = $this->db->prepare('SELECT * FROM Accounts WHERE Id = :id');
		$stmt->bindParam(':id', $id);
		$stmt->execute();
		$stmt->setFetchMode(PDO::FETCH_CLASS, 'User');
		return $stmt->fetch();
	}

	/**
	 * @param $username string
	 * @return integer Id matching given username or 0 if no user found
	 */
	public function getIdByName($username) {
		$stmt = $this->db->prepare("SELECT Id FROM Accounts WHERE Username = :username");
		$stmt->bindValue(':username', $username);
		$stmt->execute();

		$ret = $stmt->fetch();

		if ($ret && !empty($ret['Id'])) {
			return intval($ret['Id']);
		}

		return 0;
	}

	/**
	 * Check if user's password matches the given password.
	 *
	 * @param $id integer
	 * @param $password string Clear text password to be checked
	 * @return bool Whether password matches
	 */
	public function checkPassword($id, $password) {
		$stmt = $this->db->prepare('SELECT Password FROM Accounts WHERE Id = :id');
		$stmt->bindParam(':id', $id);
		$stmt->execute();

		$ret = $stmt->fetch();

		if ($ret && !empty($ret['Password'])) {
			return crypt($password, $ret['Password']) === $ret['Password'];
		}

		return false;
	}

	/**
	 * Add a new user.
	 *
	 * @param $username string
	 * @param $password string User's clear text password (will be hashed here)
	 * @param $email string
	 * @param $level integer An integer representing the user's access group
	 * @return integer User's ID
	 */
	public function addUser($username, $password, $email, $level) {
		$password = $this->hashPassword($password);
		$stmt = $this->db->prepare("
			INSERT INTO Accounts (Username, Password, Email, Level)
			VALUES (:username, :password, :email, :level)");

		$stmt->bindParam(':username', $username);
		$stmt->bindParam(':password', $password);
		$stmt->bindParam(':email', $email);
		$stmt->bindParam(':level', $level);
		$stmt->execute();

		return $this->getIdByName($username);
	}

	public function deleteUser($id) {
		$stmt = $this->db->prepare('DELETE FROM Accounts WHERE Id = :id');
		$stmt->bindParam(':id', $id);
		$stmt->execute();
	}

	/**
	 * Update an existing user's data.
	 *
	 * @param $id integer
	 * @param $username string
	 * @param $email string
	 * @param $level integer
	 */
	public function updateUser($id, $username, $email, $level) {
		$stmt = $this->db->prepare("
			UPDATE Accounts
			SET
				Username = :username,
				Email = :email,
				Level = :level
			WHERE Id = :id");

		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':username', $username);
		$stmt->bindParam(':email', $email);
		$stmt->bindParam(':level', $level);
		$stmt->execute();
	}

	/**
	 * Change user's password
	 * @param $id integer User's ID
	 * @param $password string Clear text password
	 */
	public function changePassword($id, $password) {
		$password = $this->hashPassword($password);
		$stmt = $this->db->prepare("UPDATE Accounts SET Password = :password WHERE Id = :id");

		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':password', $password);
		$stmt->execute();
	}

	/**
	 * Ban a user for a specified amount of time, setting their group
	 * 0 (Banned) and un-ban time to current time + length.
	 *
	 * @param $id integer User's ID
	 * @param $length integer Length of the ban (in seconds), 0 = permanent
	 */
	public function banUser($id, $length) {
		$stmt = $this->db->prepare("UPDATE Accounts SET BanTime = :length, Level = 0 WHERE Id = :id");
		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':length', $length);
	}

	public function unBanUser($id) {
		$stmt = $this->db->prepare("UPDATE Accounts SET BanTime = -1, Level = 1 WHERE Id = :id");
		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':length', $length);
	}
}