<?php

require_once(APP . 'lib/model.php');
require_once(APP . 'models/online.php');

class ChallengeModel extends MemcacheModel
{
	public function challenge($db, $ownUser, $ownTeam, $targetTeam) {
		$teamModel = new TeamModel($db);
		$userModel = new UserModel($db);
		$online = new OnlineModel($this->mc);

		$ownTeam = $teamModel->getTeamData($ownTeam);
		$targetTeam = $teamModel->getTeamData($targetTeam);

		if ($ownTeam->User !== $ownUser) {
			return false;
		}

		$target = $userModel->getUser($targetTeam->User);

		// Can't challenge yourself
		if ($ownUser === $target->Id) {
			return false;
		}

		// Only challenge online and not busy users
		if ($online->getStatus($target->Id) !== UserStatus::Online) {
			return false;
		}
		if ($online->getStatus($ownUser) !== UserStatus::Online) {
			return false;
		}

		// Only add challenge if one isn't already active
		if ($this->getChallenge($ownUser) || $this->getChallenge($target->Id)) {
			return false;
		}
		if ($this->getChallengedId($ownUser) || $this->getChallenge($target->Id)) {
			return false;
		}

		$this->mc->set('fartwall-challenger-' . $ownUser, $target->Id, 0, 15);
		$res = $this->mc->set('fartwall-challenge-' . $target->Id, array(
			'sender' => $ownUser,
			'senderTeam' => $ownTeam->Id,
			'targetTeam' => $targetTeam->Id
		), 0, 15);

		return $res ? $this->getChallenge($target->Id) : false;
	}

	public function answer($target, $accepted) {
		$chl = $this->mc->get('fartwall-challenge-' . $target);

		if ($accepted && $chl) {
			$this->mc->set('fartwall-accepted-' . $chl['sender'], $accepted, 0, 15);
			$this->mc->set('fartwall-accepted-' . $target, $accepted, 0, 15);
		} else {
			$this->mc->delete('fartwall-accepted-' . $chl['sender']);
			$this->mc->delete('fartwall-accepted-' . $target);
		}

		$this->mc->delete('fartwall-challenge-' . $target);
		$this->mc->delete('fartwall-challenger-' . $chl['sender']);
	}

	public function getAccepted($id) {
		return $this->mc->get('fartwall-accepted-' . $id);
	}

	public function getChallengedId($id) {
		return $this->mc->get('fartwall-challenger-' . $id);
	}

	public function getChallenge($id) {
		return $this->mc->get('fartwall-challenge-' . $id);
	}
}