<?php
require_once(APP . 'lib/model.php');

class Gladiator
{
	public $Id;
	public $Team;
	public $InStore;
	public $Name;
	public $Race;
	public $Salary;
	public $Age;
	public $Constitution;
	public $Strength;
	public $Agility;
	public $Wisdom;
	public $Dodge;
	public $MagicResistance;
	public $FistSkill;
	public $SwordSkill;
	public $MaceSkill;
	public $SpearSkill;
	public $BowSkill;
	public $DestructionSkill;
	public $RestorationSkill;
	public $AlterationSkill;
	public $IllusionSkill;
	public $MeleeWeapon;
	public $RangedWeapon;
	public $Armor;
	public $SpellOne;
	public $SpellTwo;
	public $SpellThree;
	public $Coma;

	public function __construct()
	{
		$this->Id = intval($this->Id);
		$this->Team = intval($this->Team);
		$this->Salary = intval($this->Salary);
		$this->Age = intval($this->Age);
		$this->Constitution = intval($this->Constitution);
		$this->Strength = intval($this->Strength);
		$this->Agility = intval($this->Agility);
		$this->Wisdom = intval($this->Wisdom);
		$this->Dodge = intval($this->Dodge);
		$this->MagicResistance = intval($this->MagicResistance);
		$this->FistSkill = intval($this->FistSkill);
		$this->SwordSkill = intval($this->SwordSkill);
		$this->MaceSkill = intval($this->MaceSkill);
		$this->SpearSkill = intval($this->SpearSkill);
		$this->BowSkill = intval($this->BowSkill);
		$this->DestructionSkill = intval($this->DestructionSkill);
		$this->RestorationSkill = intval($this->RestorationSkill);
		$this->AlterationSkill = intval($this->AlterationSkill);
		$this->IllusionSkill = intval($this->IllusionSkill);
		$this->Coma = intval($this->Coma);
	}
}

class GladiatorModel extends Model
{
	/**
	 * @param $teamId
	 * @return Array of Gladiators in a Team.
	 */
	public function getTeamsGladiators($teamId)
	{
		$stmt = $this->db->prepare('SELECT * FROM Gladiators WHERE Team = :teamId AND InStore = 0');
		$stmt->bindParam('teamId', $teamId);
		$stmt->execute();
		$stmt->setFetchMode(PDO::FETCH_CLASS, 'Gladiator');

		return $stmt->fetchAll();
	}

	public function getInStoreGladiatorsByTeamId($teamid)
	{
		$stmt = $this->db->prepare('SELECT * FROM Gladiators WHERE InStore = 1 AND Team = :teamid');
		$stmt->bindParam(':teamid', $teamid);
		$stmt->execute();
		$stmt->setFetchMode(PDO::FETCH_CLASS, 'Gladiator');

		return $stmt->fetchAll();
	}

	public function deleteInStoreGladiators($team) {
		$stmt = $this->db->prepare('
			DELETE FROM Gladiators
			WHERE InStore = 1 AND Team = :team');

		$stmt->bindParam(':team', $team);
		$stmt->execute();
	}

	public function getAllGladiators()
	{
		$stmt = $this->db->prepare('SELECT * FROM Gladiators');
		$stmt->execute();
		$stmt->setFetchMode(PDO::FETCH_CLASS, 'Gladiator');

		return $stmt->fetchAll();
	}

	/**
	 * @param gladiator 's name string
	 * @return Integer Gladiators's ID if gladiator exists otherwise return int 0.
	 */
	public function getGladiatorIdByName($name)
	{
		$stmt = $this->db->prepare('SELECT Id FROM Gladiators WHERE Name = :name');
		$stmt->bindParam(':name', $name);
		$stmt->execute();
		$ret = $stmt->fetch();

		if ($ret && !empty($ret['Id'])) {
			return $ret['Id'];
		}

		return 0;
	}


	/**
	 * @param $id integer
	 * @return Gladiator
	 */
	public function getGladiator($id)
	{
		$stmt = $this->db->prepare('SELECT * FROM Gladiators WHERE Id = :id');
		$stmt->bindParam(':id', $id);
		$stmt->execute();
		$stmt->setFetchMode(PDO::FETCH_CLASS, 'Gladiator');
		return $stmt->fetch();
	}

	/**
	 * @param $id integer
	 * @param $teamid integer
	 * @param $instore boolean
	 * @param $age integer
	 * @param $constitution integer
	 * @param $strength integer
	 * @param $agility integer
	 * @param $wisdom integer
	 * @param $dodge integer
	 * @param $magicres integer
	 * @param $fistskill integer
	 * @param $swordskill integer
	 * @param $maceskill integer
	 * @param $spearskill integer
	 * @param $bowskill integer
	 * @param $destructionskill integer
	 * @param $restorationskill integer
	 * @param $alterationskill integer
	 * @param $illusionskill integer
	 * @param $coma integer
	 */
	public function updateGladiator($id, $teamid, $instore, $age, $constitution, $strength, $agility, $wisdom, $dodge,
									$magicres, $fistskill, $swordskill, $maceskill, $spearskill, $bowskill, $destructionskill,
									$restorationskill, $alterationskill, $illusionskill, $meleeweapon, $rangedweapon, $spellone, $spelltwo, $spellthree, $coma)
	{
		$stmt = $this->db->prepare("
			UPDATE Gladiators
			SET
				Team = :teamid,
				InStore = :instore,
				Age = :age,
				Constitution = :constitution,
				Strength = :strength,
				Agility = :agility,
				Wisdom = :wisdom,
				Dodge = :dodge,
				MagicResistance = :magicres,
				FistSkill = :fistskill,
				SwordSkill = :swordskill,
				MaceSkill = :maceskill,
				SpearSkill = :spearskill,
				BowSkill = :bowskill,
				DestructionSkill = :destructionskill,
				RestorationSkill = :restorationskill,
				AlterationSkill = :alterationskill,
				IllusionSkill = :illusionskill,
				MeleeWeapon = :meleeweapon,
				RangedWeapon = :rangedweapon,
				SpellOne = :spellone,
				SpellTwo = :spelltwo,
				SpellThree = :spellthree,
				Coma = :coma
			WHERE Id = :id");

		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':teamid', $teamid);
		$stmt->bindParam(':instore', $instore);
		$stmt->bindParam(':age', $age);
		$stmt->bindParam(':constitution', $constitution);
		$stmt->bindParam(':strength', $strength);
		$stmt->bindParam(':agility', $agility);
		$stmt->bindParam(':wisdom', $wisdom);
		$stmt->bindParam(':dodge', $dodge);
		$stmt->bindParam(':magicres', $magicres);
		$stmt->bindParam(':fistskill', $fistskill);
		$stmt->bindParam(':swordskill', $swordskill);
		$stmt->bindParam(':maceskill', $maceskill);
		$stmt->bindParam(':spearskill', $spearskill);
		$stmt->bindParam(':bowskill', $bowskill);
		$stmt->bindParam(':destructionskill', $destructionskill);
		$stmt->bindParam(':restorationskill', $restorationskill);
		$stmt->bindParam(':alterationskill', $alterationskill);
		$stmt->bindParam(':illusionskill', $illusionskill);
		$stmt->bindParam(':meleeweapon', $meleeweapon);
		$stmt->bindParam(':rangedweapon', $rangedweapon);
		$stmt->bindParam(':spellone', $spellone);
		$stmt->bindParam(':spelltwo', $spelltwo);
		$stmt->bindParam(':spellthree', $spellthree);
		$stmt->bindParam(':coma', $coma);

		$stmt->execute();
	}


	/**
	 * @param $teamid integer
	 * @param $instore boolean
	 * @param $name string
	 * @param $race string
	 * @param $age integer
	 * @param $salary integer
	 * @param $constitution integer
	 * @param $strength integer
	 * @param $agility integer
	 * @param $wisdom integer
	 * @param $dodge integer
	 * @param $magicres integer
	 * @param $fistskill integer
	 * @param $swordskill integer
	 * @param $maceskill integer
	 * @param $spearskill integer
	 * @param $bowskill integer
	 * @param $destructionskill integer
	 * @param $restorationskill integer
	 * @param $alterationskill integer
	 * @param $illusionskill integer
	 * @param $coma integer
	 * @return int Gladiator's ID
	 */
	public function addGladiator($teamid, $instore, $name, $race, $age, $salary, $constitution, $strength, $agility, $wisdom, $dodge,
								 $magicres, $fistskill, $swordskill, $maceskill, $spearskill, $bowskill, $destructionskill,
								 $restorationskill, $alterationskill, $illusionskill, $meleeweapon, $rangedweapon, $spellone, $spelltwo, $spellthree, $coma)
	{
		$stmt = $this->db->prepare("
			INSERT INTO Gladiators (
				Team,
				InStore,
				Name,
				Race,
				Age,
				Salary,
				Constitution,
				Strength,
				Agility,
				Wisdom,
				Dodge,
				MagicResistance,
				FistSkill,
				SwordSkill,
				MaceSkill,
				SpearSkill,
				BowSkill,
				DestructionSkill,
				RestorationSkill,
				AlterationSkill,
				IllusionSkill,
				MeleeWeapon,
				RangedWeapon,
				SpellOne,
				SpellTwo,
				SpellThree,
				Coma)
			VALUES (:teamid, :instore, :name, :race, :age, :salary, :constitution, :strength, :agility, :wisdom, :dodge, :magicres,
				:fistskill, :swordskill, :maceskill, :spearskill, :bowskill, :destructionskill, :restorationskill, :alterationskill,
				:illusionskill, :meleeweapon, :rangedweapon, :spellone, :spelltwo, :spellthree,:coma)");

		$stmt->bindParam(':teamid', $teamid);
		$stmt->bindParam(':instore', $instore);
		$stmt->bindParam(':name', $name);
		$stmt->bindParam(':race', $race);
		$stmt->bindParam(':age', $age);
		$stmt->bindParam(':salary', $salary);
		$stmt->bindParam(':constitution', $constitution);
		$stmt->bindParam(':strength', $strength);
		$stmt->bindParam(':agility', $agility);
		$stmt->bindParam(':wisdom', $wisdom);
		$stmt->bindParam(':dodge', $dodge);
		$stmt->bindParam(':magicres', $magicres);
		$stmt->bindParam(':fistskill', $fistskill);
		$stmt->bindParam(':swordskill', $swordskill);
		$stmt->bindParam(':maceskill', $maceskill);
		$stmt->bindParam(':spearskill', $spearskill);
		$stmt->bindParam(':bowskill', $bowskill);
		$stmt->bindParam(':destructionskill', $destructionskill);
		$stmt->bindParam(':restorationskill', $restorationskill);
		$stmt->bindParam(':alterationskill', $alterationskill);
		$stmt->bindParam(':illusionskill', $illusionskill);
		$stmt->bindParam(':meleeweapon', $meleeweapon);
		$stmt->bindParam(':rangedweapon', $rangedweapon);
		$stmt->bindParam(':spellone', $spellone);
		$stmt->bindParam(':spelltwo', $spelltwo);
		$stmt->bindParam(':spellthree', $spellthree);
		$stmt->bindParam(':coma', $coma);

		$stmt->execute();
		return $this->getGladiatorIdByName($name);
	}
	public function buyGladiator($id){
		$stmt = $this->db->prepare('UPDATE Gladiators SET Instore = 0 WHERE Id = :id');
		$stmt->bindParam(':id', $id);
		$stmt->execute();
	}

	public function deleteGladiator($id)
	{
		$stmt = $this->db->prepare('DELETE FROM Gladiators WHERE Id = :id');
		$stmt->bindParam(':id', $id);
		$stmt->execute();
	}

}


