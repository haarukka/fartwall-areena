<?php

require_once(APP . 'lib/model.php');

abstract class UserStatus {
	const Offline = 0;
	const Online = 1;
	const Busy = 2;
}

class OnlineModel extends MemcacheModel
{
	public function __construct($mc) {
		parent::__construct($mc);

		$onlines = $this->mc->get('fartwall-online');
		if (!$onlines) {
			$this->mc->set('fartwall-online', array());
		}
	}

	public function setStatus($id, $status) {
		$this->mc->set('fartwall-status-' . $id, $status, 0, 30);
		$onlines = $this->mc->get('fartwall-online');
		$onlines[$id] = $status;
		$this->mc->replace('fartwall-online', $onlines);
	}

	private function getOnlyStatus($id) {
		return $this->mc->get('fartwall-status-' . $id);
	}

	public function getStatus($id) {
		$st = $this->getOnlyStatus($id);

		$onlines = $this->mc->get('fartwall-online');
		if ($st === UserStatus::Offline) {
			unset($onlines[$id]);
		} else {
			$onlines[$id] = $st;
		}
		$this->mc->replace('fartwall-online', $onlines);

		return $st;
	}

	public function getAll() {
		$all = $this->mc->get('fartwall-online');
		$newAll = array();
		foreach($all as $id => $status) {
			$st = $this->getOnlyStatus($id);

			if ($st && $st !== UserStatus::Offline) {
				$newAll[$id] = $st;
			}
		}
		$this->mc->replace('fartwall-online', $newAll);
		return $newAll;
	}
}