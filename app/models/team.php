<?php

require_once(APP . 'lib/model.php');

class Team
{
	public $Id;
	public $User;
	public $Name;
	public $Money;
	public $Battles;
	public $Wins;

	public function __construct()
	{
		$this->Id = intval($this->Id);
		$this->User = intval($this->User);
		$this->Money = intval($this->Money);
		$this->Battles = intval($this->Battles);
		$this->Wins = intval($this->Wins);
	}

}
class TeamModel extends Model
{
	/**
	 * @param $teamid integer Team's ID
	 * @return Team
	 */
	public function getTeamData($teamid)
	{
		$stmt = $this->db->prepare('SELECT * FROM Teams WHERE Id = :teamid');
		$stmt->bindParam(':teamid', $teamid);
		$stmt->execute();
		$stmt->setFetchMode(PDO::FETCH_CLASS, 'Team');
		return $stmt->fetch();
	}

	public function getAllTeams()
	{
		$stmt = $this->db->prepare('SELECT * FROM Teams ORDER BY Wins DESC');
		$stmt->execute();
		$stmt->setFetchMode(PDO::FETCH_CLASS, 'Team');

		return $stmt->fetchAll();
	}


	/**
	 * @param $teamname
	 * @return integer Id matching given team's name, int 0 if team's name is not found.
	 */
	public function getTeamIdByName($teamname) {
		$stmt = $this->db->prepare("SELECT Id FROM Teams WHERE Name = :teamname");
		$stmt->bindValue(':teamname', $teamname);
		$stmt->execute();

		$ret = $stmt->fetch();

		if ($ret && !empty($ret['Id'])) {
			return $ret['Id'];
		}

		return 0;
	}

	/**
	 * @param $userId integer
	 * @return Team
	 */
	public function getTeamsByUserId($userId){
		$stmt = $this->db->prepare('SELECT * FROM Teams WHERE User = :userId');
		$stmt->bindParam(':userId', $userId);
		$stmt->execute();
		$stmt->setFetchMode(PDO::FETCH_COLUMN, 3);
		return $stmt->fetchAll();
	}


	public function deleteTeam($teamid)
	{
		$stmt = $this->db->prepare('DELETE FROM Teams WHERE Id = :teamid');
		$stmt->bindParam(':teamid', $teamid);
		$stmt->execute();
	}


	/**
	 * @param $userid Integer User's ID
	 * @param $teamname string
	 * @return Integer Team's ID
	 */
	public function addTeam($userid, $teamname)
	{
		$stmt = $this->db->prepare("
					INSERT INTO Teams (User, Name, Money, Battles, Wins)
					VALUES (:userid, :teamname, 2000, 0, 0)");
		$stmt->bindParam(':userid', $userid);
		$stmt->bindParam(':teamname', $teamname);
		$stmt->execute();

		return $this->getTeamIdByName($teamname);
	}


	/**
	 * @param $teamid integer
	 * @param $battles integer
	 * @param $money integer
	 * @param $wins integer
	 */
	public function updateTeam($teamid, $money, $battles, $wins)
	{
		$stmt = $this->db->prepare("
			UPDATE Teams
			SET
				Battles = :battles,
				Wins = :wins,
				Money = :money
			WHERE Id = :teamid");

		$stmt->bindParam(':teamid', $teamid);
		$stmt->bindParam(':battles', $battles);
		$stmt->bindParam(':wins', $wins);
		$stmt->bindParam(':money', $money);
		$stmt->execute();
	}
}