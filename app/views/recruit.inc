 <div id="tabContainerSchool">
 	<?php if ($this->logged): ?>
	<div id="recruitable">
		
				<div class="recGlad">
					<div class="recruitableGladiator" id="nro1">
						<p></p>
					</div>
					<input class="recBut" id="rec1" type="submit" value="Värvää">
				</div>
				<div class="recGlad">
					<div class="recruitableGladiator" id="nro2">
						<p></p>
					</div>
					<input class="recBut" id="rec2" type="submit" value="Värvää">
				</div>
				<div class="recGlad">
					<div class="recruitableGladiator" id="nro3">
						<p></p>
					</div>
					<input class="recBut" id="rec3" type="submit" value="Värvää">
				</div>
				<div class="recGlad">
					<div class="recruitableGladiator" id="nro4">
						<p></p>
					</div>
					<input class="recBut" id="rec4" type="submit" value="Värvää">
				</div>
				<div class="recGlad">
					<div  class="recruitableGladiator" id="nro5">
						<p></p>
					</div>
					<input class="recBut" id="rec5" type="submit" value="Värvää">
				</div>
	</div>
	<div id="recInfo">
		<div id="infoBlock">
			<div class="infoGladStat1">
				<p class="infoGladName infoFirstStats"></p>
				<img class="firstStats" src="content/images/age.png"/>
				<p class="infoGladAge infoFirstStats"></p>
				<img class="firstStats" src="content/images/salary.png"/>
				<p class="infoGladSalary infoFirstStats"></p>
			</div>
			<div class="infoGladStat2">
				<div id="block1">
					<p class="recStrength">Voima:</p>
					<p class="recConstitution">Kesto:</p>
				</div>
				<div id="block2">
					<p class="recAgility">Nopeus:</p>
					<p class="recWisdom">Viisaus:</p>
				</div>
			</div>
			<div class="infoGladStat3">
				<p class="recDodge">Väistö:</p>
				<p class="recMagicResistance">Loitsunvastustus:</p>
			</div>
		</div>

		<div id="infoBlock">
			<div id="text">
				<h5>Asetaidot</h5>
				<p>Nyrkki:</p>
				<p>Keihäs:</p>
				<p>Miekka:</p>
				<p>Nuija:</p>
				<p>Jousi:</p>
			</div>
			<div id="number">
				<h5>Osuminen</h5>
				<p class="recFist"></p>
				<p class="recSpear"></p>
				<p class="recSword"></p>
				<p class="recMace"></p>
				<p class="recBow"></p>
			</div>


		</div>
	</div>
	<?php endif; ?>
</div>







