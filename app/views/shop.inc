<p>
	<div id="tabs2">
		<ul>
			<li><a href="#tabs-21">Jouset</a></li>
			<li><a href="#tabs-22">Keihäät</a></li>
			<li><a href="#tabs-23">Miekat</a></li>
			<li><a href="#tabs-24">Nuijat</a></li>
			<li><a href="#tabs-25">Panssarit</a></li>
		</ul>
		<div id="tabs-21">
			<div class="tabContainer">
				<table>
					<tr>
						<td>Nimi</td>
						<td>Vahinko</td>
						<td>Hinta</td>
						<td></td>
					</tr>
					<?php foreach($this->weapons['bows'] as $x => $x_value):?>
					<tr>
						<td class="td1"><?php $name = $this->escape($name = $x_value->getName()); echo "$name"; ?></td>
						<td><?php $dmg = $x_value->getDamage(); echo $dmg[0] . " - " . $dmg[1]; ?></td>
						<td><?php $price = $x_value->getPrice(); echo "$price";?></td>
						<td class="ranged" id= <?= $x ;?>><?php if ($this->logged): ?>Osta<?php endif; ?></td>
					</tr>
					<?php endforeach; ?>
				</table>
			</div>
		</div>
		<div id="tabs-22">
			<div class="tabContainer">
				<table>
					<tr>
						<td>Nimi</td>
						<td>Vahinko</td>
						<td>Torjunta</td>
						<td>Hinta</td>
						<td></td>
					</tr>
					<?php foreach($this->weapons['spears'] as $x => $x_value):?>
					<tr>
						<td class="td1"><?php $name = $this->escape($name = $x_value->getName()); echo "$name"; ?></td>
						<td><?php $dmg = $x_value->getDamage(); echo $dmg[0] . " - " . $dmg[1]; ?></td>
						<td><?php $block = $x_value->getBlock(); echo $block*100 . " %";?></td>
						<td><?php $price = $x_value->getPrice(); echo "$price";?></td>
						<td class="melee" id= <?= $x ;?>><?php if ($this->logged): ?>Osta<?php endif; ?></td>
					</tr>
					<?php endforeach; ?>
				</table>
			</div>
		</div>
		<div id="tabs-23">
			<div class="tabContainer">
				<table>
					<tr>
						<td>Nimi</td>
						<td>Vahinko</td>
						<td>Torjunta</td>
						<td>Hinta</td>
						<td></td>
					</tr>
					<?php foreach($this->weapons['swords'] as $x => $x_value):?>
					<tr>
						<td class="td1"><?php $name = $this->escape($name = $x_value->getName()); echo "$name"; ?></td>
						<td><?php $dmg = $x_value->getDamage(); echo $dmg[0] . " - " . $dmg[1]; ?></td>
						<td><?php $block = $x_value->getBlock(); echo $block*100 . " %";?></td>
						<td><?php $price = $x_value->getPrice(); echo "$price";?></td>
						<td class="melee" id= <?= $x ;?>><?php if ($this->logged): ?>Osta<?php endif; ?></td>
					</tr>
					<?php endforeach; ?>
				</table>
			</div>
		</div>
		<div id="tabs-24">
			<div class="tabContainer">
				<table>
					<tr>
						<td>Nimi</td>
						<td>Vahinko</td>
						<td>Torjunta</td>
						<td>Hinta</td>
						<td></td>
					</tr>
					<?php foreach($this->weapons['maces'] as $x => $x_value):?>
					<tr>
						<td class="td1"><?php $name = $this->escape($name = $x_value->getName()); echo "$name"; ?></td>
						<td><?php $dmg = $x_value->getDamage(); echo $dmg[0] . " - " . $dmg[1]; ?></td>
						<td><?php $block = $x_value->getBlock(); echo $block*100 . " %";?></td>
						<td><?php $price = $x_value->getPrice(); echo "$price";?></td>
						<td class="melee" id= <?= $x ;?>><?php if ($this->logged): ?>Osta<?php endif; ?></td>
					</tr>
					<?php endforeach; ?>
				</table>
			</div>
		</div>
		<div id="tabs-25">
			<div class="tabContainer">
				<table>
					<tr>
						<td>Nimi</td>
						<td>Suojaus</td>
						<td>Hinta</td>
						<td></td>
					</tr>
					<?php foreach($this->armors as $x => $x_value):?>
					<tr>
						<td class="td1"><?php $name = $this->escape($name = $x_value->getName()); echo "$name"; ?></td>
						<td><?php $ac = $x_value->getAc(); echo "$ac";?></td>
						<td><?php $price = $x_value->getPrice(); echo "$price";?></td>
						<td class="armor" id= <?= $x ;?>><?php if ($this->logged): ?>Osta<?php endif; ?></td>
					</tr>
					<?php endforeach; ?>
				</table>
			</div>
		</div>
	</div>
</p>