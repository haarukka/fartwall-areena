<!DOCTYPE html>

<html>
<head>
	<meta charset="UTF-8">
	<title>Fartwall Areena</title>
	<link rel="stylesheet" href="content/css/index.css"/>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/smoothness/jquery-ui.css" />
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
	<script type="text/javascript" src="content/js/index.js"></script>
	<!--suppress JSDuplicatedDeclaration -->
	<script>
	<?php if (isset($_SESSION['selectedTeam'])): ?>
		var selectedTeam = '<?= $this->escape($_SESSION['selectedTeam']); ?>';
	<?php else: ?>
		var selectedTeam = false;
	<?php endif; ?>
	</script>
</head>

<body>
	<header>
		<img id="logo" src="content/images/logo.png"/>
		<img id="kuvalogo" src="content/images/gladiaattorit.png"/>
		<div id="links">
			<?php if ($this->logged): ?>
				<?php if ($this->admin): ?>
					<a class="headerLink" href="admin.php">Admin</a>
				<?php endif; ?>
				<a class="headerLink" id="logOut" href="ajax/logout.php">Kirjaudu ulos</a>
			<?php else: ?>
				<a class="headerLink" id="logIn" href="#">Kirjaudu sisään</a>
				<a class="headerLink" id="signUp" href="#">Luo tili</a>
			<?php endif; ?>
		</div>
	</header>

	<section class="own">
		<p id="tabhdr">Omat<br>Joukkueesi</p>
		<div id="ownTeamList">
			<?php if ($this->logged): ?>
				<?php foreach ($this->teams as $row): ?>
					<div class="team">
						<p class="ownTeamName"><?= $row ?></p>
					</div>
				<?php endforeach; ?>
				<button id="newTeamButton">Uusi joukkue</button>
			<?php endif; ?>
		</div>
	</section>

	<div id="tabs">
  		<ul>
    		<li><a href="#tabs-1">Värvää</a></li>
    		<li><a href="#tabs-2">Kauppa</a></li>
			<li><a href="#tabs-3">Maagi</a></li>
	   		<li><a href="#tabs-4">Koulu</a></li>
  		</ul>

		<div id="tabs-1">
			<?= $this->render("recruit") ?>
		</div>

		<div id="tabs-2">
			<?= $this->render("shop") ?>
		</div>

		<div id="tabs-3">
			<?= $this->render("spellshop") ?>
		</div>

		<div id="tabs-4">
			<?= $this->render("school") ?>
		</div>
	</div>

	<section id="teamInfo">
		<h2 id="teamName">Ei valittua joukkuetta</h2>
		<button id="teamChallenge">Haasta</button>
		<div id="teamStats">
			<img class="stat" src="content/images/squirrelskin.png"/>
			<p class="stat" id="teamMoney">0</p>
			<img class="stat" src="content/images/wins.png"/>
			<p class="stat" id="teamWins">0</p>
			<img class="stat" src="content/images/losses.png"/>
			<p class="stat" id="teamLosses">0</p>
		</div>
		<table id="gladiators">
			<tr class="gladiatorRow">
				<td class="gladiator"></td>
				<td class="gladiator"></td>
				<td class="gladiator"></td>
			</tr>
			<tr class="gladiatorRow">
				<td class="gladiator"></td>
				<td class="gladiator"></td>
				<td class="gladiator"></td>
			</tr>
		</table>

		<div id="gladInfo">
			<div id="gladStat1">
				<p class="firstStats" id="gladName">Placeholder</p>
				<img class="firstStats" src="content/images/age.png"/>
				<p class="firstStats" id="gladAge">0</p>
				<img class="firstStats" src="content/images/salary.png"/>
				<p class="firstStats" id="gladSalary">0</p>
				<img class="firstStats" id="weaponSkills" src="content/images/menu.png"/>
			</div>
			<div id="gladStat2">
				<div id="block1">
					<p>Voima: <span id="gladStr">0</span></p>
					<p>Kesto: <span id="gladCon">0</span></p>
				</div>
				<div id="block2">
					<p>Nopeus: <span id="gladAgi">0</span></p>
					<p>Viisaus: <span id="gladWis">0</span></p>
				</div>
			</div>
			<div class="gear">
				<p id="gladMelee">Meleease</p>
			</div>

			<div class="gear">
				<p id="gladRanged">Jousiase</p>
			</div>

			<div class="gear">
				<p id="gladArmor">Panssari</p>
			</div>

			<div class="spell">
				<p id="gladSpell1">Loitsu</p>
			</div>

			<div class="spell">
				<p id="gladSpell2">Loitsu</p>
			</div>

			<div class="spell">
				<p id="gladSpell3">Loitsu</p>
			</div>
		</div>
	</section>

	<section class="enemy">
		<p id="tabhdr">Joukkuelista</p>
		<div id="enemyTeamList">
			<?php if ($this->logged):
				foreach ($this->allteams as $team): ?>
					<div class="enemyTeam user<?= $team->User ?>" id="team<?= $team->Id ?>">
						<div class="enemyTeamContainer">
						<p class="enemyTeamName"><?= $this->escape($team->Name) ?></p>
						<p class="enemyTeamScore"><?= $team->Wins ?></p>
						</div>
					</div>
				<?php endforeach;
			endif; ?>
		</div>
	</section>

	<div id="dialog" title="Kirjaudu sisään">
		<?= $this->render("login") ?>
	</div>

	 <div id="dialog2" title="Luo uusi tili">
		 <?= $this->render("register") ?>
	</div>

	<div id="dialog3" title="Luo uusi joukkue">
		<?= $this->render("newteam") ?>
	</div>

	<div id="dialog4" title="Gladiaattorin taidot">
		<?= $this->render("skills") ?>
	</div>

	<div id="challengerDialog" title="Odotetaan...">
		<?= $this->render("challenger") ?>
	</div>

	<div id="challengeDialog" title="Sait haasteen!">
		<?= $this->render("challenge") ?>
	</div>
</body>


</html>
