<!DOCTYPE html>

<html>
<head>
	<meta charset="UTF-8">
	<title>Fartwall Areena admin</title>
</head>

<body>
	<header>
	</header>

	<table id="users">
		<thead>
			<tr>
				<th>Username</th>
				<th>Email</th>
			</tr>
		</thead>
		<tbody>
<?php foreach ($this->users as $user): ?> 
			<tr>
				<td><?= $this->escape($user->Username) ?></td>
				<td><?= $this->escape($user->Email) ?></td>
				<td><a href="admin.php?edit=<?= $user->Id ?>">Edit</a></td>
				<td>
					<form action="admin.php" method="post">
						<input type="hidden" name="action" value="delete">
						<input type="hidden" name="id" value="<?= $user->Id ?>">
						<input type="submit" value="Delete">
					</form>
				</td>
			</tr>
<?php endforeach; ?>
		</tbody>
	</table>
</body>

</html>
