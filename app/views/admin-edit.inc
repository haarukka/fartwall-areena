<!DOCTYPE html>

<html>
<head>
	<meta charset="UTF-8">
	<title>Fartwall Areena admin</title>
</head>

<body>
	<header>
	</header>

	<h3>Editing user <?= $this->escape($this->user->Username) ?></h3>

	<form name="edit" action="admin.php" method="POST" accept-charset="utf-8">
		<input type="hidden" name="action" value="edit">
		<input type="hidden" name="id" value="<?= $this->user->Id ?>">

		<label for="username">Käyttäjätunnus:</label>
		<br>
		<input type="text" name="username" value="<?= $this->escape($this->user->Username) ?>" pattern=".{1,20}[0-9A-Za-zåäöÅÄÖ]+" required>
		<br><br>
		<label for="password">Salasana:</label>
		<br>
		<input type="password" id="pass" name="password" pattern=".{8,}" title="Salasanassa oltava vähintään 8 merkkiä." autocomplete="off">
		<br><br>

		<label for="email">Sähköposti:</label>
		<br>
		<input type="email" name="email" value="<?= $this->escape($this->user->Email) ?>" required>
		<br><br>

		<label for="level">Taso:</label>
		<br>
		<input type="number" name="level" value="<?= $this->user->Level ?>" required>
		<br><br>

		<label for="ban">Jäädytysaika:</label>
		<br>
		<input type="number" name="ban" value="<?= $this->user->BanTime ?>" required>
		<br><br>

		<input id="editButton" type="submit" value="Edit user">
	</form>
</body>

</html>
