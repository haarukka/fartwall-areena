<form name="login" action="ajax/login.php" method="post" accept-charset="utf-8">
	<span id="loginError"></span>
	<label>Käyttäjätunnus:</label>
	<br>
	<input type="text" name="username" pattern=".{1,20}[0-9A-Za-zåäöÅÄÖ]+" required>
	<br><br>
	<label for="password">Salasana:</label>
	<br>
	<input type="password" name="password" pattern=".{8,}" title="Salasanassa oltava vähintään 8 merkkiä." autocomplete="off" required>
	<br><br>
	<input id="loginButton" type="submit" value="Kirjaudu sisään">
</form>