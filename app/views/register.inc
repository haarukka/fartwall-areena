<form name="signup" action="ajax/register.php" method="POST" accept-charset="utf-8">
	<label for="username">Käyttäjätunnus:</label>
	<br>
	<input type="text" name="username" pattern=".{1,20}[0-9A-Za-zåäöÅÄÖ]+" required>
	<br><br>
	<label for="password">Salasana:</label>
	<br>
	<input type="password" id="pass" name="password" pattern=".{8,}" title="Salasanassa oltava vähintään 8 merkkiä." autocomplete="off" required>
	<br><br>
	<label for="password">Vahvista salasana:</label>
	<br>
	<input type="password" id="pass2" oninput="check(this)" name="password2" pattern=".{8,}" autocomplete="off" required>
	<br><br>

	<!-- Checks if passwords are matching-->
	<script language='javascript' type='text/javascript'>
		function check(input) {
			if (input.value != document.getElementById('pass').value) {
				input.setCustomValidity("Salasanat eivät täsmää");
			} else {
				// Input is valid -- reset the error message
				input.setCustomValidity('');
			}
		}
	</script>

	<label for="email">Sähköposti:</label>
	<input type="email" name="email" required>
	<br><br>
	<input id="signupButton" type="submit" value="Luo tili">
</form>