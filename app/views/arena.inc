<html>
<head>
	<meta charset="UTF-8">
	<title>Fartwall Areena</title>
	<link type="text/css" rel="stylesheet" href="content/css/arena.css"/>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/smoothness/jquery-ui.css" />
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
	<script type="text/javascript" src="content/js/arena.js"></script>
	<script>
		var battleId = <?= $this->battleId ?>;
		var interactive = <?= $this->interactive ? 'true' : 'false' ?>;
		var ownTeam = <?= '' . $this->ownTeam ?>;
	</script>
</head>
<body>
<img id="logo" src="content/images/logo.png"/>

<div id="own" class="team">
	<h2><?= $this->escape($this->leftTeam->Name) ?></h2>
	<?php foreach($this->leftGlad as $glad): ?>
		<div class="arenaGlad">
			<span class="arenaName"><?= $this->escape($glad->Name) ?></span>
			<br>
			<span class="arenaMana">
				<?= $glad->Mana ?>/<?= $glad->Wisdom ?>
			</span>
			<span class="arenaHP">
				<?= $glad->HP ?>/<?= $glad->Constitution ?>
			</span>
		</div>
		<br>
	<?php endforeach; ?>
</div>

<div id="center">
	<div id="main">
		<div id="arena">
			<table>
				<?php for($y = 0; $y < 9; $y++): ?>
				<tr>
					<?php for($x = 0; $x < 12; $x++): ?>
					<td class="fieldCell"></td>
					<?php endfor; ?>
				</tr>
				<?php endfor; ?>
			</table>
		</div>
		<button id="forfeitButton">Luovuta</button>
	</div>

	<div id="chat">
		<div id="content">
			<?= $this->log ?>
		</div>
	<?php if ($this->interactive): ?>
		<div id="input">
			<input type="text" name="say" id="say">
			<input type="submit" id="chatButton" value=">">
		</div>
	<?php endif; ?>
	</div>
</div>

<div id="enemy" class="team">
	<h2><?= $this->escape($this->rightTeam->Name) ?></h2>
	<?php foreach($this->rightGlad as $glad): ?>
		<div class="arenaGlad">
			<span class="arenaName"><?= $this->escape($glad->Name) ?></span>
			<br>
			<span class="arenaHP">
				<span><?= $glad->HP ?></span>/<?= $glad->Constitution ?>
			</span>
			<span class="arenaMana">
				<?= $glad->Mana ?>/<?= $glad->Wisdom ?>
			</span>
		</div>
		<br>
	<?php endforeach; ?>
</div>
</body>
</html>