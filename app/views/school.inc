<div id="tabContainerSchool">
	<?php if ($this->logged): ?>
		<div id="container">
			<div class="button strength">
				<p>Bodaa voimaa</p>
			</div>
			<div class="button wisdom">
				<p>Harjoita viisautta</p>
			</div>
			<div class="button agility">
				<p>Treenaa nopeutta</p>
			</div>
			<div class="button constitution">
				<p>Koettele kestoa</p>
			</div>
		</div>
	<?php else: ?>
		<h4 style="margin:20% 20%">Täällä ei ole mitään tehtävää.</h4>
	<?php endif; ?>
</div>