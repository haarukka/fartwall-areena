<form name="newTeam" action="ajax/team.php" method="post" accept-charset="utf-8">
	<input type="hidden" name="action" value="new">
	<label>Joukkueen nimi:</label>
	<br>
	<input type="text" name="teamname" pattern=".{1,20}[0-9A-Za-zåäöÅÄÖ]+" required>
	<br><br>
	<input id="createTeamButton" type="submit" value="Luo joukkue">
</form>