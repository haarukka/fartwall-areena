<p>
	<div id="tabs3">
		<ul>
			<li><a href="#tabs-31">Parannus</a></li>
			<li><a href="#tabs-32">Vahinko</a></li>
			<li><a href="#tabs-33">Siunaus</a></li>
			<li><a href="#tabs-34">Hämäys</a></li>
		</ul>
		<div id="tabs-31">
			<div class="tabContainer">
				<table>
					<tr>
						<td>Nimi</td>
						<td>Parannus</td>
						<td>Hinta</td>
						<td></td>
					</tr>
					<?php foreach($this->spells['healSpells'] as $x => $x_value):?>
					<tr>
						<td><?php $name = $this->escape($name = $x_value->getName()); echo "$name"; ?></td>
						<td><?php $healamount = $x_value->getHealAmount(); echo "$healamount"; ?></td>
						<td><?php $price = $x_value->getPrice(); echo "$price";?></td>
						<td class="buyspell" id= <?= $x ;?>><?php if ($this->logged): ?>Osta<?php endif; ?></td>
					</tr>
					<?php endforeach; ?>
				</table>
			</div>
		</div>
		<div id="tabs-32">
			<div class="tabContainer">
				<table>
					<tr>
						<td>Nimi</td>
						<td>Vahinko</td>
						<td>Hinta</td>
						<td></td>
					</tr>
					<?php foreach($this->spells['damageSpells'] as $x => $x_value):?>
					<tr>
						<td><?php $name = $this->escape($name = $x_value->getName()); echo "$name"; ?></td>
						<td><?php $dmg = $x_value->getDamage(); echo $dmg[0] .  " - " . $dmg[1]; ?></td>
						<td><?php $price = $x_value->getPrice(); echo "$price";?></td>
						<td class="buyspell" id= <?= $x ;?>><?php if ($this->logged): ?>Osta<?php endif; ?></td>
					</tr>
					<?php endforeach; ?>
				</table>
			</div>
		</div>
		<div id="tabs-33">
			<div class="tabContainer">
				<table>
					<tr>
						<td>Nimi</td>
						<td>Buffi</td>
						<td>Hinta</td>
						<td></td>
					</tr>
					<?php foreach($this->spells['buffSpells'] as $x => $x_value):?>
					<tr>
						<td><?php $name = $this->escape($name = $x_value->getName()); echo "$name"; ?></td>
						<td><?php $buff = $x_value->getBuffAmount(); echo "$buff"; ?></td>
						<td><?php $price = $x_value->getPrice(); echo "$price";?></td>
						<td class="buyspell" id= <?= $x ;?>><?php if ($this->logged): ?>Osta<?php endif; ?></td>
					</tr>
					<?php endforeach; ?>
				</table>
			</div>
		</div>
		<div id="tabs-34">
			<div class="tabContainer">
				<table>
					<tr>
						<td>Nimi</td>
						<td>Hämääntymisaika</td>
						<td>Hinta</td>
						<td></td>
					</tr>
					<?php foreach($this->spells['illusionSpells'] as $x => $x_value):?>
					<tr>
						<td><?php $name = $this->escape($name = $x_value->getName()); echo "$name"; ?></td>
						<td><?php $stunTime = $x_value->getStunTime(); echo "$stunTime"; ?></td>
						<td><?php $price = $x_value->getPrice(); echo "$price";?></td>
						<td class="buyspell" id= <?= $x ;?>><?php if ($this->logged): ?>Osta<?php endif; ?></td>
					</tr>
					<?php endforeach; ?>
				</table>
			</div>
		</div>
	</div>
</p>