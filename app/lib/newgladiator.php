<?php

require_once(APP . 'models/gladiators.php');
require_once(APP . 'game/gladiators.php');

function regenerateGladiators($db, $team)
{
	$gladmdl = new GladiatorModel($db);

	$gladmdl->deleteInStoreGladiators($team);

	for ($i = 1; $i <= 5; $i++) {
		$gladiator = new Gladiators($db);

		$gladmdl->addGladiator($team, $gladiator->inStore, $gladiator->name, $gladiator->race, $gladiator->age, $gladiator->salary,
		$gladiator->constitution, $gladiator->strength, $gladiator->agility, $gladiator->wisdom,
		$gladiator->dodge, $gladiator->magicRes, $gladiator->fist, $gladiator->sword, $gladiator->mace,
		$gladiator->spear, $gladiator->bow, $gladiator->destruction, $gladiator->restoration, $gladiator->alteration,
		$gladiator->illusion, $gladiator->meleeweapon, $gladiator->rangedweapon, $gladiator->spellone, $gladiator->spelltwo, $gladiator->spellthree, $gladiator->coma);
	}
}