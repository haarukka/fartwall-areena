<?php

class Template {
	/**
	 * Defines the base path for templates.
	 *
	 * {APP}/views/ by default.
	 * @var string Where templates are located
	 */
	private $templatePath;

	/**
	 * @var array Contains all the variables to be passed to the template
	 */
	public $vars;

	public function __construct() {
		$this->templatePath = APP . 'views/';
		$this->vars = array();
	}

	/**
	 * Render a given template into HTML.
	 *
	 * @param $template string Name of the template to render
	 * @return string Full output of the given template
	 * @throws Exception If the given template does not exist
	 */
	public function render($template) {
		$fullPath = $this->templatePath . $template . '.inc';

		// Buffer output until the entire template is rendered
		ob_start();

		if (file_exists($fullPath)) {
			include($fullPath);
		} else {
			throw new Exception('Template not found');
		}

		echo "\n";

		return ob_get_clean();
	}

	/**
	 * Escape string's HTML entities
	 *
	 * @param $str string Raw string
	 * @return string Encoded string
	 */
	public function escape($str) {
		return htmlentities($str);
	}

	// Override __set and __get for slightly shorter variable setting
	public function __set($k, $v) {
		$this->vars[$k] = $v;
	}

	public function __get($k) {
		return $this->vars[$k];
	}
}