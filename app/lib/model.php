<?php

class Model {
	protected $db;

	/**
	 * @param $db PDO
	 */
	public function __construct($db) {
		$this->db = $db;
	}

	/**
	 * @return PDO
	 */
	public function getDb() {
		return $this->db;
	}
}

class MemcacheModel {
	protected $mc;

	/**
	 * @param $mc Memcache
	 */
	public function __construct($mc) {
		$this->mc = $mc;
	}
}