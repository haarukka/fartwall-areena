<?php

class Session {
	private $username;

	/**
	 * Start a session.
	 * Should be called at the beginning of every page.
	 */
	function __construct() {
		session_name('fartwall_session');
		session_start();
	}

	/**
	 * @return string Session's username.
	 */
	function getUsername() {
		return $this->username;
	}

	/**
	 * Set session's username. Should only be needed when logging in.
	 *
	 * @param $username string Session's username.
	 */
	function setUsername($username) {
		$this->username = $username;
	}

	/**
	 * @param $userModel UserModel
	 * @param bool $fatal
	 * @return integer User ID if session is valid, 0 otherwise
	 */
	function check($userModel, $fatal=true) {
		if (!$this->isLoggedIn()) {
			if ($fatal) {
				die('You need to be logged in');
			} else {
				return 0;
			}
		}

		$id = $userModel->getIdByName($this->username);

		if ($id === 0) {
			$this->logout();
			if ($fatal) {
				die('Non-existent user');
			} else {
				return 0;
			}
		}

		return $id;
	}

	/**
	 * Check whether user is currently logged in.
	 *
	 * @return bool Whether user is logged in.
	 */
	function isLoggedIn() {
		if (isset($_SESSION['username'])) {
			$this->username = $_SESSION['username'];
			return true;
		}
		return false;
	}

	/**
	 * Log user in by saving their username in the session array.
	 */
	function login() {
		// TODO: Add partial IP checking? (first two bytes probably)
		$_SESSION['username'] = $this->username;
	}

	/**
	 * Log user out and destroy their session array.
	 */
	function logout() {
		$_SESSION = array();
		session_destroy();
	}
}