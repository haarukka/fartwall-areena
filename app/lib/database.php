<?php

function openDatabaseConnection() {
	global $config;

	$opts = $config['db']['options'];
	$db = new PDO($opts);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$db->exec('PRAGMA FOREIGN_KEYS = ON');

	return $db;
}

function openMemcacheConnection() {
	global $config;

	$mc = new Memcache();
	$mc->connect($config['mc']['host'], $config['mc']['port'])
		or die('Could not connect to memcache');

	return $mc;
}