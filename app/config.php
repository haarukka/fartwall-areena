<?php

// Constant pointing to project root ("/srv/www/lut/" for example)
define('ROOT', dirname(__DIR__) . '/');

// App root, containing private .php files
define('APP', ROOT . 'app/');

// Content root, containing static files
define('CONTENT', ROOT . 'content/');

// Game root
define('GAME', APP . 'game/');

return array(
	'db' => array(
		// Options string passed to the PDO constructor
		'options' => 'sqlite:' . APP . 'fartwall.db'
	),
	'mc' => array(
		'host' => 'localhost',
		'port' => 11211
	)
);