<?php

$config = require('app/config.php');
require(APP . 'lib/database.php');
require(APP . 'lib/session.php');
require(APP . 'lib/template.php');

require(APP . 'models/user.php');

$session = new Session();

$db = openDatabaseConnection();

$mdl = new UserModel($db);

$id = $session->check($mdl);

$user = $mdl->getUser($id);

if ($user->Level !== UserGroup::Admin) {
	echo 'You need to be an admin';
	exit();
}

$method = $_SERVER['REQUEST_METHOD'];

if ($method == 'POST') {
	// Edit the user

	$action = $_POST['action'];

	if ($action === 'edit') {
		if (isset($_POST['id']) && is_numeric($_POST['id'])) {
			$id = $_POST['id'];
			$user = $mdl->getUser($_POST['id']);

			if (!$user) {
				echo 'Invalid user ID';
				exit();
			}

			$username = $_POST['username'];
			$password = $_POST['password'];
			$email = $_POST['email'];
			$level = $_POST['level'];
			$ban = $_POST['ban'];

			if (!is_numeric($level)) {
				$level = $user->Level;
			} else {
				$level = intval($level);
			}

			if (!is_numeric($ban)) {
				$ban = $user->BanTime;
			} else {
				$ban = intval($ban);
			}

			if (!empty($password)) {
				$mdl->changePassword($id, $password);
			}

			if ($ban !== $user->BanTime) {
				if ($ban === -1) {
					$mdl->unBanUser($id);
				} else {
					$mdl->banUser($id, $ban);
					$level = UserGroup::Banned;
				}
			}

			$mdl->updateUser($id, $username, $email, $level);
			header('Location: admin.php');
		}
	} elseif ($action === 'delete') {
		if (isset($_POST['id']) && is_numeric($_POST['id'])) {
			$id = $_POST['id'];
			$user = $mdl->getUser($_POST['id']);

			if (!$user) {
				echo 'Invalid user ID';
				exit();
			}

			$mdl->deleteUser($id);
			header('Location: admin.php');
		}
	}

} else {
	// Display user list or edit page

	$admin = new Template();

	if (isset($_GET['edit']) && is_numeric($_GET['edit'])) {
		$admin->user = $mdl->getUser($_GET['edit']);

		if (!$admin->user) {
			echo 'Invalid user ID';
			exit();
		}

		echo $admin->render('admin-edit');
	} else {
		$admin->users = $mdl->getAllUsers();

		echo $admin->render('admin');
	}
}