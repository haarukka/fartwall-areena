<?php

$config = require('app/config.php');

require(APP . 'lib/database.php');
require(APP . 'lib/session.php');
require(APP . 'lib/template.php');
require(APP . 'models/user.php');
require(APP . 'models/team.php');
require(APP . 'models/gladiators.php');

if (!file_exists(APP . 'fartwall.db')) {
	echo 'Run install.php first!';
	exit();
}

$session = new Session();

$db = openDatabaseConnection();
$mdl = new UserModel($db);

$id = $session->check($mdl, false);
$logged = $id !== 0;

$teammdl = new TeamModel($db);

$gladiator = new GladiatorModel($db);

$user = $mdl->getUser($id);

$index = new Template();
$index->title = 'Fartwall Areena';
$index->admin = $user && ($user->Level === UserGroup::Admin);
$index->logged = $logged;
$index->username = $session->getUsername();

$teamid = $teammdl->getTeamIdByName($_SESSION['selectedTeam']);

$index->glads = $gladiator->getInStoreGladiatorsByTeamId($teamid);

$index->weapons = require(APP . 'game/weapons.php');
$index->armors = require(APP . 'game/armors.php');
$index->spells = require(APP . 'game/spells.php');

$index->teams = $teammdl->getTeamsByUserId($id);
$index->allteams = $teammdl->getAllTeams();


echo $index->render('index');