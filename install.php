<?php

$config = require('app/config.php');

require(APP . 'lib/database.php');

if (!file_exists(APP . 'fartwall.db')) {
	$db = openDatabaseConnection();
	$sql = file_get_contents(APP . 'fartwall.sql');
	$db->exec($sql);

	echo 'Database imported';
} else {
	header('Location: index.php');
}