"use strict";

// Login dialog
function logIn() {
	$("#dialog").css("display", "initial").dialog({
		autoOpen: false,
		modal: true,
		resizable: false,
		width: "370"
	});
}


// Sign up dialog
function signUp() {
	$("#dialog2").css("display", "initial").dialog({
		autoOpen: false,
		modal: true,
		resizable: false,
		width: "370"
	});
}

// Dialog for creating a new team
function newTeam() {
	$("#dialog3").css("display", "initial").submit(function () {
		$("#dialog3").dialog("close");
	}).dialog({
		autoOpen: false,
		modal: true,
		resizable: false,
		width: "370"
	});
}

function skills() {
	$("#dialog4").css("display", "initial").submit(function () {
		$("#dialog4").dialog("close");
	}).dialog({
		autoOpen: false,
		modal: false,
		resizable: false,
		width: "400"
	});
}

// Deletes teams from own teamlist and enemy teamlist when logging out
function logOut() {
	$("#team").remove();
	$("#enemyTeam").remove();
}

var teamData;
var selectedGlad;

var cellSlots = {
	'#gladName': 'Name',
	'#gladAge': 'Age',
	'#gladSalary': 'Salary',
	'#gladStr': 'Strength',
	'#gladCon': 'Constitution',
	'#gladWis': 'Wisdom',
	'#gladAgi': 'Agility',
	'#gladMelee': 'MeleeWeapon',
	'#gladRanged': 'RangedWeapon',
	'#gladArmor': 'Armor',
	'#gladSpell1': 'SpellOne',
	'#gladSpell2': 'SpellTwo',
	'#gladSpell3': 'SpellThree'
};

function displayGladiator(idx) {
	if (!teamData) return;
	var glad = teamData['glads'][idx];

	if (!glad) return;

	selectedGlad = glad['Id'];

	$('.selectedGladiator').removeClass('selectedGladiator');
	$('.gladiator').eq(idx).addClass('selectedGladiator');

	for (var k in cellSlots) {
		if (cellSlots.hasOwnProperty(k)) {
			$(k).text(glad[cellSlots[k]]);
		}
	}
}

function selectGladiator() {
	displayGladiator($('.gladiator').index(this));
}

function displayTeam(data) {
	data = JSON.parse(data);
	teamData = data;

	$('#teamName').text(data['team']['Name']);
	$('#teamMoney').text(data['team']['Money']);
	$('#teamWins').text(data['team']['Wins']);
	$('#teamLosses').text(data['team']['Battles'] - data['team']['Wins']);

		var x = 1;
		for (var printthis in data['inStoreGlads'] ) {
			var i = x.toString();
			$("#nro"+ i +"").text(data['inStoreGlads'][printthis]['Name']);
			x = x + 1;
		}

	var gladI = 0;
	$('#gladiators').find('td').text('');
	data['glads'].forEach(function(g) {
		$('#gladiators').find('td').eq(gladI).text(g['Name']);
		gladI++;
	});

	displayGladiator(0);
}

function selectRecGlad() {

	$('.selectedRecGlad').removeClass('selectedRecGlad');
	$(this).addClass('selectedRecGlad');

	var teamName = $(".selectedOwn").find('p:first').text();
	$.post('ajax/team.php', {
		action: 'select',
		teamname: teamName
	});

	$.get('ajax/team.php', {
			teamname: teamName
		},
		displayInfo
	);
}

function displayInfo(data) {
	data = JSON.parse(data);
	teamData = data;


	var b = $(".selectedRecGlad").attr("id");
	b = parseInt(b.slice(3,4));

	$('.recStrength').text("Voima: " + data['inStoreGlads'][b-1]['Strength']);
	$('.recConstitution').text("Kesto: " + data['inStoreGlads'][b-1]['Constitution']);
	$('.recAgility').text("Nopeus: " + data['inStoreGlads'][b-1]['Agility']);
	$('.recWisdom').text("Viisaus: " + data['inStoreGlads'][b-1]['Wisdom']);
	$('.recDodge').text("Väistö: " + data['inStoreGlads'][b-1]['Dodge']);
	$('.recMagicResistance').text("Loitsunvastustus: " + data['inStoreGlads'][b-1]['MagicResistance']);
	$('.recFist').text(data['inStoreGlads'][b-1]['FistSkill']);
	$('.recSpear').text(data['inStoreGlads'][b-1]['SpearSkill']);
	$('.recSword').text(data['inStoreGlads'][b-1]['SwordSkill']);
	$('.recMace').text(data['inStoreGlads'][b-1]['MaceSkill']);
	$('.recBow').text(data['inStoreGlads'][b-1]['BowSkill']);
	$('.infoGladSalary').text(data['inStoreGlads'][b-1]['Salary']);
	$('.infoGladAge').text(data['inStoreGlads'][b-1]['Age']);
	$('.infoGladName').text(data['inStoreGlads'][b-1]['Name']);

	var gladI = 0;
	$('#gladiators').find('td').text('');
	data['glads'].forEach(function(g) {
		$('#gladiators').find('td').eq(gladI).text(g['Name']);
		gladI++;
	});

	displayGladiator(0);
}

function selectOwnTeam() {
	$('.selectedOwn').removeClass('selectedOwn');
	$(this).addClass('selectedOwn');

	var teamName = $(this).find('p:first').text();
	$.post('ajax/team.php', {
		action: 'select',
		teamname: teamName
	});

	$.get('ajax/team.php', {
			teamname: teamName
		},
		displayTeam
	);
}

function selectOtherTeam() {
	$('.selectedOther').removeClass('selectedOther');
	$(this).addClass('selectedOther');

	var teamName = $(this).find('p:first').text();

	$.get('ajax/team.php', {
			teamname: teamName
		},
		displayTeam
	);
}

function challengeTeam() {
	var teamName = $('#teamName').text();

	$.post('ajax/challenge.php', {
			action: 'challenge',
			targetTeam: teamName
		},
		function(data) {
			data = JSON.parse(data);

			if (data) {
				$('#challengerOwnTeam').text(data['ownTeam']);
				$('#challengerEnemyTeam').text(data['senderTeam']);

				$('#challengerDialog').dialog('open');
			}
		});
}

function stayOnline() {
	setTimeout(stayOnline, 15000);

	$.post('ajax/online.php');
}

function updateOnlineUsers() {
	setTimeout(updateOnlineUsers, 15000);

	$.get('ajax/online.php',
		{},
		function(data) {
			data = JSON.parse(data);

			$('.online, .busy')
				.removeClass('online')
				.removeClass('busy');

			$.each(data, function(k, v) {
				if (v === 1) {
					$('.user' + k).addClass('online');
				} else if (v === 2) {
					$('.user' + k).addClass('busy');
				}
			});
		});
}

function checkChallenges() {
	setTimeout(checkChallenges, 5000);

	$.get('ajax/challenge.php',
		{},
		function(data) {
			data = JSON.parse(data);

			if (data['accepted']) {
				window.location = './arena.php?id=' + data['accepted'];
			}

			var $chl = $('#challengeDialog');

			if (!data['ownTeam']) {
				if (!data['waiting']) {
					$('#challengerDialog').dialog('close');
					$chl.dialog('close');
				}

				return;
			}

			$('#challengeOwnTeam').text(data['ownTeam']);
			$('#challengeEnemyTeam').text(data['senderTeam']);

			$chl.dialog('open');
		});
}

function buy() {
	$.post('ajax/buy.php', {
		action: 'buy',
		type: $(this).attr("class"),
		teamname: $(".selectedOwn").find('p:first').text(),
		item: $(this).attr("id"),
		gladiatorId: selectedGlad
	}
)};

function recruit() {

	$('.clicked').removeClass('clicked');
	$(this).addClass('clicked');

	var teamName = $(".selectedOwn").find('p:first').text();
	$.post('ajax/team.php', {
		action: 'select',
		teamname: teamName
	});

	$.get('ajax/team.php', {
			teamname: teamName
		},
		sendRecruit
	);

}


function sendRecruit(data) {
	data = JSON.parse(data);
	teamData = data;
	var recGladId = $(".clicked").attr("id");
	recGladId = parseInt(recGladId.slice(3,4));
	recGladId = data['inStoreGlads'][recGladId-1]['Id'];

	$.post('ajax/recruit.php', {
		action: 'recruit',
		id: recGladId
	});
	$("#nro" + recGladId +"").empty();
}

$(function () {
	$("#tabs").tabs({
		beforeLoad: function (event, ui) {
			ui.jqXHR.error(function () {
				ui.panel.html("Couldn't load this tab.");
			});
		}
	});

	$("#tabs2").tabs({
		beforeLoad: function (event, ui) {
			ui.jqXHR.error(function () {
				ui.panel.html("Couldn't load this tab.");
			});
		}
	});

	$("#tabs3").tabs({
		beforeLoad: function (event, ui) {
			ui.jqXHR.error(function () {
				ui.panel.html("Couldn't load this tab.");
			});
		}
	});

	logIn();
	signUp();
	newTeam();
	logOut();
	skills();

	stayOnline();
	updateOnlineUsers();
	checkChallenges();

	$('#challengeDialog').dialog({
		autoOpen: false,
		modal: false,
		resizable: false,
		width: "400"
	});

	$('#challengerDialog').dialog({
		autoOpen: false,
		modal: false,
		resizable: false,
		width: "400"
	});

	$("#logIn").on("click", function () {
		$("#dialog").dialog("open");
	});

	$("form[name='login']").submit(function(e) {
		e.preventDefault();

		$.post('ajax/login.php', {
			username: $(this).find('input[name="username"]').val(),
			password: $(this).find('input[name="password"]').val(),
			json: 1
		}, function(data) {
			data = JSON.parse(data);
			if (!data['login']) {
				$('#loginError').text('Kirjautuminen epäonnistui');
			} else {
				window.location.reload();
			}
		});
	});

	$('#acceptChallenge').click(function() {
		$.post('ajax/challenge.php', {
			action: 'accept'
		}, function(data) {
			if (data.length > 0) {
				var id = parseInt(data);
				if (!isNaN(id)) {
					window.location = './arena.php?id=' + id;
				}
			}
		});
	});

	$('#rejectChallenge').click(function() {
		$.post('ajax/challenge.php', {
			action: 'reject'
		}, function() {
			$('#challengeDialog').dialog('close');
		});
	});

	$('#cancelChallenge').click(function() {
		$.post('ajax/challenge.php', {
			action: 'cancel'
		}, function() {
			$('#challengerDialog').dialog('close');
		});
	});

	$('#teamChallenge').click(challengeTeam);

	$("#signUp").on("click", function () {
		$("#dialog2").dialog("open");
	});

	$("#logOut").on("click", function () {
		logOut();
	});


	$(".strength").on("click", function () {
		alert("voima");
	});

	$(".wisdom").on("click", function () {
		alert("viisaus");
	});

	$(".agility").on("click", function () {
		alert("nopeus");
	});

	$(".constitution").on("click", function () {
		alert("kesto");
	});

	$(".recruitableGladiator").on("click", selectRecGlad);

	$(".team").on("click", selectOwnTeam);
	$(".enemyTeam").on("click", selectOtherTeam);

	$(".gladiator").on("click", selectGladiator);

	if (selectedTeam) {
		var $team = $('.ownTeamName:contains(' + selectedTeam + ')').parent();
		if ($team[0]) {
			selectOwnTeam.call($team[0]);
		}
	}

	$("#newTeamButton").on("click", function () {
		$("#dialog3").dialog("open");
	});

	$("#weaponSkills").on("click", function () {
		$("#dialog4").dialog("open");
	});

	$(".melee").on("click", buy);
	$(".ranged").on("click", buy);
	$("tr .spell").on("click", buy);

	$(".recBut").on("click", recruit);
});
