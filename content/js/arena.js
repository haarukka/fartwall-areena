var currentGlad;
var ownTurn = false;
var lastUpdate = 0;
var currentData;
var gladAmount;

var server = (function($) {

	this.move = function move(x, y) {
		$.post(
			'ajax/battle.php',
			{
				id: battleId,
				action: 'move',
				x: x,
				y: y
			},
			update
		)
	};

	this.melee = function melee(x, y) {
		$.post(
			'ajax/battle.php',
			{
				id: battleId,
				action: 'melee',
				x: x,
				y: y
			},
			update
		)
	};

	return this;
}(jQuery));

function getGladTurn(turn) {
	return ((turn - 1) % gladAmount) + 1
}

function updateStatus(glad) {
	$('.arenaName:contains("' + glad['Gladiator']['Name'] + '")')
		.parent()
		.find('.arenaHP span').text(glad['HP']);
}

function addGladiator(glad, sprite, turn) {
	var $map = $('#arena').find('table');
	var $cell = $($map[0].rows[glad['PosY']].cells[glad['PosX']]);

	var $sprite = $('<img src="content/sprites/' + sprite + '.png">');
	$cell.append($sprite);

	updateStatus(glad);

	if (getGladTurn(turn) === glad['Turn']) {
		currentGlad = glad;
		$cell.addClass('currentTurn');
		$('.team div:contains("' + glad['Gladiator']['Name'] + '")')
			.addClass('currentTurn');

		if (glad['Gladiator']['Team'] === ownTeam) {
			ownTurn = true;
		}
	}
}

function checkGlads(x, y) {
	var ret = false;

	currentData['glads'].forEach(function(glad) {
		if (glad['PosX'] === x && glad['PosY'] === y) {
			ret = glad;
		}
	});

	return ret;
}

function checkEnemy(x, y) {
	var ret = false;

	currentData['glads'].forEach(function(glad) {
		if (glad['Gladiator']['Team'] !== ownTeam
			&& glad['PosX'] === x && glad['PosY'] === y) {

			ret = glad;
		}
	});

	return ret;
}

function isInRange(x, y, range) {
	return (Math.abs(x - currentGlad['PosX']) <= range
	     && Math.abs(y - currentGlad['PosY']) <= range);
}

function canMove(x, y) {
	if (checkGlads(x, y)) {
		return false;
	}

	return isInRange(x, y, 1);
}

function canMelee(x, y) {
	var enemy = checkEnemy(x, y);

	if (!enemy) return false;

	return isInRange(enemy['PosX'], enemy['PosY'], 1);
}

function hoverEnterCell() {
	if (!ownTurn) return;

	var idx = $('.fieldCell').index(this);
	var x = idx % 12;
	var y = Math.floor(idx/12);

	var hoverGlad = checkGlads(x, y);

	if (hoverGlad) {
		$('.arenaName:contains("' + hoverGlad['Gladiator']['Name'] + '")')
			.parent().addClass('hoverGlad');
	}

	if (canMove(x, y)) {
		$(this).css('cursor', 'move');
	} else if (canMelee(x, y)) {
		$(this).css('cursor', 'pointer');
	}
}

function hoverLeaveCell() {
	if (!ownTurn) return;

	$('.hoverGlad').removeClass('hoverGlad');
	$(this).css('cursor', 'default');
}

function clickCell() {
	if (!ownTurn) return;

	var idx = $('.fieldCell').index(this);
	var x = idx % 12;
	var y = Math.floor(idx/12);

	if (canMove(x, y)) {
		server.move(x, y);
	} else if (canMelee(x, y)) {
		server.melee(x, y);
	}
}

function onSuccess(data) {
	setTimeout(update, 5000);

	data = JSON.parse(data);

	if (!data['battle']) {
		window.location = 'index.php';
	}

	var team1 = data['battle']['Team1'];
	var team2 = data['battle']['Team2'];
	var turn = data['battle']['Turn'];

	// Only update when necessary
	if (lastUpdate >= turn) return;

	currentData = data;
	lastUpdate = turn;
	ownTurn = false;
	gladAmount = data['glads'].length;

	$('#arena').find('td').each(function() {
		$(this).empty();
	});
	$('.currentTurn').removeClass('currentTurn');
	data['glads'].forEach(function(glad) {
		addGladiator(
			glad,
			glad['Gladiator']['Team'] == team1 ? 'team1' : 'team2',
			turn
		);
	});

	if (ownTurn) {
	}
}

function update() {
	$.get(
		'ajax/battle.php',
		{
			id: battleId,
			action: 'update'
		},
		onSuccess
	);
}

function stayOnline() {
	setTimeout(stayOnline, 15000);

	$.post('ajax/online.php');
}

$(function() {
	'use strict';

	update();
	stayOnline();

	$('#arena')
		.find('td')
		.hover(hoverEnterCell, hoverLeaveCell)
		.click(clickCell);

	$('#forfeitButton').click(function() {
		$.post('ajax/battle.php', {
			id: battleId,
			action: 'forfeit'
		}, function() {
			window.location = 'index.php';
		})
	})
});