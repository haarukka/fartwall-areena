<?php

$config = require('app/config.php');

require(APP . 'lib/database.php');
require(APP . 'lib/session.php');
require(APP . 'lib/template.php');
require(APP . 'models/user.php');
require(APP . 'models/team.php');
require(APP . 'models/battle.php');
require(APP . 'game/battle.php');

if (!file_exists(APP . 'fartwall.db')) {
	echo 'Run install.php first!';
	exit();
}

$method = $_SERVER['REQUEST_METHOD'];

$session = new Session();

$db = openDatabaseConnection();

$mdl = new UserModel($db);
$battleMdl = new BattleModel($db);
$teamMdl = new TeamModel($db);

$id = $session->check($mdl, false);
$logged = $id !== 0;

$user = $mdl->getUser($id);

$battleId = 0;
if ($method === 'GET') {
	$battleId = is_numeric($_GET['id']) ? intval($_GET['id']) : 0;
} else {
	$battleId = is_numeric($_POST['id']) ? intval($_POST['id']) : 0;
}
if ($battleId === 0) {
	die('Invalid battle ID');
}

$battle = $battleMdl->getBattle($battleId);
if (!$battle) {
	die('Invalid battle ID');
}

$team1 = $battle->Team1;
$team2 = $battle->Team2;

$arena = new Template();
$arena->battleId = $battleId;
$arena->leftTeam = $teamMdl->getTeamData($team1);
$arena->rightTeam = $teamMdl->getTeamData($team2);
$arena->leftGlad = $battleMdl->getTeamGladiators($battleId, $team1);
$arena->rightGlad = $battleMdl->getTeamGladiators($battleId, $team2);

$arena->interactive = $logged && ($arena->leftTeam->User === $id
		|| $arena->rightTeam->User === $id);
if ($arena->leftTeam->User === $id) {
	$arena->ownTeam = $arena->leftTeam->Id;
} elseif ($arena->rightTeam->User === $id) {
	$arena->ownTeam = $arena->rightTeam->Id;
} else {
	$arena->ownTeam = 0;
}

$arena->log = $battle->CombatLog;

echo $arena->render('arena');