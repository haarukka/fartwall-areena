<?php

$config = require('../app/config.php');
require(APP . 'lib/database.php');
require(APP . 'lib/session.php');
require(APP . 'models/user.php');
require(APP . 'models/challenge.php');
require(APP . 'models/team.php');
require(APP . 'models/battle.php');
require(APP . 'game/battle.php');

/**
 * @param $db PDO
 * @param $mc Memcache
 * @param $teamModel TeamModel
 * @param $chl array
 * @return integer Battle Id
 */
function createBattle($db, $mc, $teamModel, $chl) {
	$ownTeam = $teamModel->getTeamData($chl['targetTeam']);
	$senderTeam = $teamModel->getTeamData($chl['senderTeam']);

	if (!$ownTeam || !$senderTeam) {
		die(json_encode(array('error' => 'Invalid team')));
	}

	$battle = new BattleModel($db);

	if ($battle->isUserInBattle($ownTeam->Id)
		|| $battle->isUserInBattle($senderTeam->Id)) {
		die(json_encode(array('error' => 'Already in battle')));
	}

	$bl = BattleLogic::createBattle($battle, $teamModel, $senderTeam, $ownTeam);

	$online = new OnlineModel($mc);
	$online->setStatus($ownTeam->Id, UserStatus::Busy);
	$online->setStatus($senderTeam->Id, UserStatus::Busy);

	return $bl->battle->Id;
}

/**
 * @param $db PDO
 * @param $chlModel ChallengeModel
 * @param $teamModel TeamModel
 * @param $id integer
 */
function challenge($db, $chlModel, $teamModel, $id) {
	if (!isset($_POST['targetTeam'])) {
		exit();
	}

	if (!isset($_SESSION['selectedTeam'])) {
		exit();
	}

	if (is_numeric($_POST['targetTeam'])) {
		$targetTeam = intval($_POST['targetTeam']);
	} else {
		$targetTeam = $teamModel->getTeamIdByName($_POST['targetTeam']);
	}

	$ownTeam  = $teamModel->getTeamIdByName($_SESSION['selectedTeam']);

	if ($targetTeam === 0) exit();
	if ($ownTeam === 0) exit();

	$chl = $chlModel->challenge($db, $id, $ownTeam, $targetTeam);

	if (!$chl) die(json_encode(false));

	$ownTeam = $teamModel->getTeamData($chl['targetTeam']);
	$senderTeam = $teamModel->getTeamData($chl['senderTeam']);

	die(json_encode(array(
		'ownTeam' => $ownTeam->Name,
		'senderTeam' => $senderTeam->Name
	)));
}

/**
 * @param $chlModel ChallengeModel
 * @param $id integer
 */
function cancelChallenge($chlModel, $id) {
	$target = $chlModel->getChallengedId($id);
	$chlModel->answer($target, false);
}

/**
 * @param $id integer
 * @param $db PDO
 * @param $mc Memcache
 * @param $chlModel ChallengeModel
 * @param $teamModel TeamModel
 */
function acceptChallenge($id, $db, $mc, $chlModel, $teamModel) {
	$chl = $chlModel->getChallenge($id);

	if (!$chl) die();

	$battleId = createBattle($db, $mc, $teamModel, $chl);

	$chlModel->answer($id, $battleId);

	die('' . $battleId);
}

/**
 * @param $id integer
 * @param $chlModel ChallengeModel
 */
function rejectChallenge($id, $chlModel) {
	$chlModel->answer($id, false);
}

$session = new Session();

$method = $_SERVER['REQUEST_METHOD'];

$db = openDatabaseConnection();
$mc = openMemcacheConnection();

$mdl = new UserModel($db);
$teamModel = new TeamModel($db);
$chlModel = new ChallengeModel($mc);

$id = $session->check($mdl);

if ($method == 'POST') {
	if (!isset($_POST['action'])) {
		if (isset($_POST['accept'])) {
			$action = 'accept';
		} elseif (isset($_POST['reject'])) {
			$action = 'reject';
		} elseif (isset($_POST['cancel'])) {
			$action = 'cancel';
		} else {
			exit();
		}
	} else {
		$action = $_POST['action'];
	}

	switch($action) {
		case 'challenge':
			challenge($db, $chlModel, $teamModel, $id);
			break;
		case 'cancel':
			cancelChallenge($chlModel, $id);
			break;
		case 'accept':
			acceptChallenge($id, $db, $mc, $chlModel, $teamModel);
			break;
		case 'reject':
			rejectChallenge($id, $chlModel);
			break;
	}
} else {
	$chl = $chlModel->getChallenge($id);

	if (!$chl) {
		$chl = $chlModel->getAccepted($id);

		if ($chl) {
			die(json_encode(array(
				'accepted' => $chl
			)));
		}

		$chl = $chlModel->getChallengedId($id);
		if ($chl) {
			die(json_encode(array(
				'waiting' => true
			)));
		}

		die(json_encode(array()));
	}

	$ownTeam = $teamModel->getTeamData($chl['targetTeam']);
	$senderTeam = $teamModel->getTeamData($chl['senderTeam']);

	echo json_encode(array(
		'ownTeam' => $ownTeam->Name,
		'senderTeam' => $senderTeam->Name
	));
}