<?php

$config = require('../app/config.php');
require(APP . 'lib/database.php');
require(APP . 'lib/session.php');
require(APP . 'models/user.php');
require(APP . 'models/online.php');
require(APP . 'models/battle.php');

$session = new Session();

$method = $_SERVER['REQUEST_METHOD'];

$mc = openMemcacheConnection();

$online = new OnlineModel($mc);

if ($method == 'POST') {
	$db = openDatabaseConnection();
	$mdl = new UserModel($db);
	$battle = new BattleModel($db);

	$id = $session->check($mdl);

	$status = UserStatus::Online;
	if ($battle->isUserInBattle($id)) {
		$status = UserStatus::Busy;
	}
	$online->setStatus($id, $status);
} else {
	if (isset($_GET['id']) && is_numeric($_GET['id'])) {
		$id = intval($_GET['id']);
		echo json_encode(array($id => $online->getStatus($id)));
	} else {
		echo json_encode($online->getAll());
	}
}