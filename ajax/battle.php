<?php

$config = require('../app/config.php');

require(APP . 'lib/database.php');
require(APP . 'lib/session.php');
require(APP . 'lib/template.php');
require(APP . 'models/user.php');
require(APP . 'models/team.php');
require(APP . 'models/battle.php');
require(APP . 'game/battle.php');

/**
 * @param $battleId
 * @param $battleMdl BattleModel
 * @return string
 */
function update($battleId, $battleMdl) {
	$ret = array(
		'battle' => $battleMdl->getBattle($battleId),
		'glads' => $battleMdl->getGladiators($battleId)
	);

	return json_encode($ret);
}

/**
 * @param $id integer
 * @param $bl BattleLogic
 * @param $teamMdl TeamModel
 */
function forfeit($id, $bl, $teamMdl) {
	$bl->forfeit($id, $teamMdl);
}

/**
 * @param $id
 * @param $bl BattleLogic
 * @param $teamMdl TeamModel
 */
function move($id, $bl, $teamMdl) {
	if (!isset($_POST['x']) || !isset($_POST['y'])) return;
	if (!is_numeric($_POST['x']) || !is_numeric($_POST['y'])) return;

	$x = intval($_POST['x']);
	$y = intval($_POST['y']);

	$bl->move($x, $y, $id, $teamMdl);
}

/**
 * @param $id
 * @param $bl BattleLogic
 * @param $teamMdl TeamModel
 */
function melee($id, $bl, $teamMdl) {
	if (!isset($_POST['x']) || !isset($_POST['y'])) return;
	if (!is_numeric($_POST['x']) || !is_numeric($_POST['y'])) return;

	$x = intval($_POST['x']);
	$y = intval($_POST['y']);

	$bl->melee($x, $y, $id, $teamMdl);
}

$method = $_SERVER['REQUEST_METHOD'];

$session = new Session();

$db = openDatabaseConnection();

$mdl = new UserModel($db);
$battleMdl = new BattleModel($db);
$teamMdl = new TeamModel($db);

$id = $session->check($mdl, false);

$user = $mdl->getUser($id);

$battleId = 0;
if ($method === 'GET' && isset($_GET['id'])) {
	$battleId = is_numeric($_GET['id']) ? intval($_GET['id']) : 0;
} elseif ($method === 'POST' && isset($_POST['id'])) {
	$battleId = is_numeric($_POST['id']) ? intval($_POST['id']) : 0;
}
if ($battleId === 0) {
	die('Invalid battle ID');
}

if ($method == 'GET') {
	if (!isset($_GET['action'])) {
		die('No action specified');
	}

	$action = $_GET['action'];

	switch($action) {
		case 'update':
			die(update($battleId, $battleMdl));
			break;
	}
} else {
	if (!isset($_POST['action'])) {
		die('No action specified');
	}

	$action = $_POST['action'];

	$bl = new BattleLogic($battleMdl, $teamMdl, $battleMdl->getBattle($battleId));

	switch($action) {
		case 'forfeit':
			forfeit($id, $bl, $teamMdl);
			break;
		case 'move':
			move($id, $bl, $teamMdl);
			break;
		case 'melee':
			melee($id, $bl, $teamMdl);
			break;
	}
}