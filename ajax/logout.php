<?php

require('../app/config.php');

require(APP . 'lib/session.php');

$session = new Session();
$session->logout();

header('Location: ../index.php');