<?php

$config = require('../app/config.php');
require(APP . 'lib/database.php');
require(APP . 'lib/session.php');
require(APP . 'models/user.php');
require(APP . 'models/team.php');
require(APP . 'models/gladiators.php');
require(APP . 'lib/newgladiator.php');

$session = new Session();

$db = openDatabaseConnection();
$mdl = new UserModel($db);

$id = $session->check($mdl);

$method = $_SERVER['REQUEST_METHOD'];

if ($method == 'POST') {
	if (!isset($_POST['action'])) exit();
	if (!isset($_POST['teamname'])) exit();

	$teamName = $_POST['teamname'];

	$teamModel = new TeamModel($db);

	switch($_POST['action']) {
		case 'new':
			if ($teamModel->getTeamIdByName($teamName) === 0) {
				$teamId = $teamModel->addTeam($id, $teamName);
				$_SESSION['selectedTeam'] = $teamName;
				regenerateGladiators($db, $teamId);
				header('Location: ../index.php');
			} else {
				break;
			}
			break;
		case 'select':
			// Only select team if user owns it
			$teamId = $teamModel->getTeamIdByName($teamName);
			$team = $teamModel->getTeamData($teamId);
			if ($team->User === $id) {
				$_SESSION['selectedTeam'] = $teamName;
			}
			break;
	}
} else {
	if (!isset($_GET['teamname'])) exit();

	$teamName = $_GET['teamname'];

	$teamModel = new TeamModel($db);
	$gladModel = new GladiatorModel($db);

	$id = $teamModel->getTeamIdByName($teamName);
	if ($id === 0) exit();

	$info = array(
		'team' => $teamModel->getTeamData($id),
		'glads' => $gladModel->getTeamsGladiators($id),
		'inStoreGlads' => $gladModel->getInStoreGladiatorsByTeamId($id)
	);

	echo json_encode($info);
}