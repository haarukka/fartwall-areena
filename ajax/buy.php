<?php
/**
 * Created by IntelliJ IDEA.
 * User: topi
 * Date: 8.12.2014
 * Time: 17:57
 */
$config = require('../app/config.php');
require(APP . 'lib/database.php');
require(APP . 'lib/session.php');
require(APP . 'models/user.php');
require(APP . 'models/team.php');
require(APP . 'models/gladiators.php');

$session = new Session();

$db = openDatabaseConnection();
$mdl = new UserModel($db);
$teammdl = new TeamModel($db);
$gladmdl = new GladiatorModel($db);

$id = $session->check($mdl);

$method = $_SERVER['REQUEST_METHOD'];

if ($method == 'POST') {
    if (!isset($_POST['action'])) exit();
    if (!isset($_POST['type'])) exit();
    if (!isset($_POST['teamname'])) exit();
    if (!isset($_POST['item'])) exit();
    if (!isset($_POST['gladiatorId'])) exit();


    $teamname = $_POST['teamname'];
    $item = $_POST['item'];
    $gladiatorId = $_POST['gladiatorId'];
    $type = $_POST['type'];
    $gladId = $gladmdl->getGladiator($gladiatorId);

    if ($_POST['action'] === 'buy') {
        if($type == 'melee'){
            $gladmdl->updateGladiator($gladId->Id, $gladId->Team, $gladId->InStore, $gladId->Name, $gladId->Race,
                $gladId->Age, $gladId->Salary, $gladId->Constitution, $gladId->Strength, $gladId->Agility,
                $gladId->Wisdom, $gladId->Dodge, $gladId->MagicResistance, $gladId->FistSkill, $gladId->SwordSkill,
                $gladId->MaceSkill, $gladId->SpearSkill, $gladId->BowSkill, $gladId->DestructionSkill,
                $gladId->RestorationSkill, $gladId->AlterationSkill, $gladId->IllusionSkill, $item,
                $gladId->RangedWeapon, $gladId->SpellOne, $gladId->SpellTwo, $gladId->SpellThree, $gladId->Coma);
        }
        if ($type == 'ranged') {
            $gladmdl->updateGladiator($gladId->Id, $gladId->Team, $gladId->InStore, $gladId->Name, $gladId->Race,
                $gladId->Age, $gladId->Salary, $gladId->Constitution, $gladId->Strength, $gladId->Agility,
                $gladId->Wisdom, $gladId->Dodge, $gladId->MagicResistance, $gladId->FistSkill, $gladId->SwordSkill,
                $gladId->MaceSkill, $gladId->SpearSkill, $gladId->BowSkill, $gladId->DestructionSkill,
                $gladId->RestorationSkill, $gladId->AlterationSkill, $gladId->IllusionSkill, $gladId->MeleeWeapon,
                $item, $gladId->SpellOne, $gladId->SpellTwo, $gladId->SpellThree, $gladId->Coma);
        }
        if ($type == 'spell'){
            if (!isset($gladId->SpellOne)){
                $gladmdl->updateGladiator($gladId->Id, $gladId->Team, $gladId->InStore, $gladId->Name, $gladId->Race,
                    $gladId->Age, $gladId->Salary, $gladId->Constitution, $gladId->Strength, $gladId->Agility,
                    $gladId->Wisdom, $gladId->Dodge, $gladId->MagicResistance, $gladId->FistSkill, $gladId->SwordSkill,
                    $gladId->MaceSkill, $gladId->SpearSkill, $gladId->BowSkill, $gladId->DestructionSkill,
                    $gladId->RestorationSkill, $gladId->AlterationSkill, $gladId->IllusionSkill, $gladId->MeleeWeapon,
                    $gladId->RangedWeapon, $item, $gladId->SpellTwo, $gladId->SpellThree, $gladId->Coma);
            }
            elseif (!isset($gladId->SpellTwo)){
                $gladmdl->updateGladiator($gladId->Id, $gladId->Team, $gladId->InStore, $gladId->Name, $gladId->Race,
                    $gladId->Age, $gladId->Salary, $gladId->Constitution, $gladId->Strength, $gladId->Agility,
                    $gladId->Wisdom, $gladId->Dodge, $gladId->MagicResistance, $gladId->FistSkill, $gladId->SwordSkill,
                    $gladId->MaceSkill, $gladId->SpearSkill, $gladId->BowSkill, $gladId->DestructionSkill,
                    $gladId->RestorationSkill, $gladId->AlterationSkill, $gladId->IllusionSkill, $gladId->MeleeWeapon,
                    $gladId->RangedWeapon, $gladId->SpellOne, $item, $gladId->SpellThree, $gladId->Coma);
            } elseif (!isset($gladId->SpellThree)){
                $gladmdl->updateGladiator($gladId->Id, $gladId->Team, $gladId->InStore, $gladId->Name, $gladId->Race,
                    $gladId->Age, $gladId->Salary, $gladId->Constitution, $gladId->Strength, $gladId->Agility,
                    $gladId->Wisdom, $gladId->Dodge, $gladId->MagicResistance, $gladId->FistSkill, $gladId->SwordSkill,
                    $gladId->MaceSkill, $gladId->SpearSkill, $gladId->BowSkill, $gladId->DestructionSkill,
                    $gladId->RestorationSkill, $gladId->AlterationSkill, $gladId->IllusionSkill, $gladId->MeleeWeapon,
                    $gladId->RangedWeapon, $gladId->SpellOne, $gladId->SpellOne , $item, $gladId->Coma);
            } else exit();
        }
    }
}

