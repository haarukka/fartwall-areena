<?php

$config = require('../app/config.php');
require(APP . 'lib/database.php');
require(APP . 'models/user.php');
require(APP . 'lib/session.php');

$session = new Session();

$method = $_SERVER['REQUEST_METHOD'];

if ($method == 'POST') {
	$email = $_POST['email'];
	$username = $_POST['username'];
	$password = $_POST['password'];

	$db = openDatabaseConnection();

	// TODO: Add validation
	$mdl = new UserModel($db);

	if ($mdl->getIdByName($username) === 0) {
		$mdl->addUser($username, $password, $email, UserGroup::Normal);

		$session->setUsername($username);
		$session->login();
	}
}

header('Location: ../index.php');