<?php
/**
 * Created by IntelliJ IDEA.
 * User: topi
 * Date: 8.12.2014
 * Time: 20:42
 */
$config = require('../app/config.php');
require(APP . 'lib/database.php');
require(APP . 'lib/session.php');
require(APP . 'models/user.php');
require(APP . 'models/team.php');
require(APP . 'models/gladiators.php');

$session = new Session();

$db = openDatabaseConnection();
$mdl = new UserModel($db);

$id = $session->check($mdl);

$method = $_SERVER['REQUEST_METHOD'];

if ($method == 'POST') {
    if (!isset($_POST['action'])) exit();
    if (!isset($_POST['id'])) exit();

    $gladid = $_POST['id'];

    $gladmdl = new GladiatorModel($db);

    $team = new TeamModel($db);
    $gladiator = $gladmdl->getGladiator($gladid);

    $gladmdl->buyGladiator($gladid);
}
header('Location: ../index.php');