<?php

$config = require('../app/config.php');
require(APP . 'lib/database.php');
require(APP . 'models/user.php');
require(APP . 'lib/session.php');

$session = new Session();

$method = $_SERVER['REQUEST_METHOD'];

if ($method == 'POST') {
	// Check credentials, create session
	$username = $_POST['username'];
	$password = $_POST['password'];
	$json = false;
	if (isset($_POST['json'])) {
		$json = true;
	}

	$db = openDatabaseConnection();
	$mdl = new UserModel($db);

	$session->setUsername($username);

	if ($mdl->checkPassword($mdl->getIdByName($username), $password)) {
		$session->login();
		if ($json) {
			die(json_encode(array('login' => true)));
		} else {
			header('Location: ../index.php');
		}
	} else {
		if ($json) {
			die(json_encode(array('login' => false)));
		} else {
			header('Location: ../index.php#loginFailed');
		}
	}
}